function opt_direction=hiropca(input_data, varargin)
%This function is the hiropca algorithm
% Input includes 'pc_no', 'lambda', 'T', 'hat_t'
% Default T is half of the sample_no
%global initial_matrix;
%global data_remaining;
%global fake_zero;
%global hat_t;

initial_matrix=input_data;
clear input_data;

observation_no=size(initial_matrix, 2);
data_remaining=ones(1, observation_no);

T=floor(observation_no/2);
hat_t_ind=-1;
lambda=0.1;

if (mod(length(varargin), 2) ~= 0 ),
    error(['Extra Parameters passed to the function ''' mfilename ''' must be passed in pairs.']);
end
parameterCount = length(varargin)/2;
for parameterIndex = 1:parameterCount,
    parameterName = varargin{parameterIndex*2 - 1};
    parameterValue = varargin{parameterIndex*2};
    switch lower(parameterName)
        case 'pc_no'
            input_d=parameterValue;
        case 'lambda'
            lambda=parameterValue;
        case 'hat_t'
            hat_t_ind=parameteValue;
        case 't'
            T=parameterValue;
 otherwise
            error(['Sorry, the parameter ''' parameterName ''' is not recognized by the function ''' mfilename '''.']);
    end
end

sample_no=floor(observation_no*(1-lambda));

if hat_t_ind == -1
   % if lambda<0.1
    if lambda==0
        hat_t=floor(0.9*sample_no);
    else 
        hat_t=sample_no;
    end
else
    hat_t=hat_t_ind;
end    

fake_zero=0.000000001;

cov_matrix=calculate_correlation(observation_no,initial_matrix,data_remaining);

OPT.disp=0;
OPT.maxit=3000;

opt_value=0;
for i=1:T
    [directions,eigen_temp]=eigs(cov_matrix, input_d, 'LM', OPT);
    temp=hiro_robust_variance(directions,hat_t,initial_matrix);
    if temp>opt_value
        opt_value=temp;
        opt_direction=directions;
        opt_no=i;
    end
    [removed_vector,data_remaining]=hiro_random_removal(directions,data_remaining,initial_matrix,fake_zero);
    cov_matrix=cov_matrix-removed_vector*removed_vector';
end

end

function total_rob_var=hiro_robust_variance(directions,hat_t,initial_matrix)
% compute the robust variance of the input set of directions
% directions is a matrix, with each of its column vector a found direction

%global hat_t;
%global initial_matrix;
                                         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
projected_value=directions'*initial_matrix; % projected_value is a matrix, %
                                           % while each column is a point
                                           % projected on the subspace given% 
                                           % by directions.
                                          % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
total_rob_var=0;
for j=1:size(projected_value, 1)
    temp=projected_value(j,:);
    for i=1:length(temp)
        temp(i)=temp(i)^2;
    end
    temp=sort(temp);                       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                           %For each direction, list the
                                           %variance in an ascending order
                                           %%%%%%%%%%%%%%%%%%%%%%%%%%%55555
                                           
    total_rob_var=total_rob_var+sum(temp(1:hat_t)); 
end    

end

function [output,data_remaining]=hiro_random_removal(directions,data_remaining,initial_matrix,fake_zero)
% randomly remove a point based on the total variance on the projected
% directions
% directions is a matrix, with each of its column vector a found direction

%global data_remaining;
%global initial_matrix;
%global fake_zero;

projected_value=directions'*initial_matrix;

total_variance=0;
projected_variance = zeros(1,size(projected_value,2));
for i=1:size(projected_value, 2)
    projected_variance(i)=0;
    for j=1:size(projected_value, 1)
       projected_variance(i)=projected_variance(i)+projected_value(j,i)^2*data_remaining(i);
    end   
    total_variance=total_variance+projected_variance(i);
end    

temp_rand=total_variance*rand(1);
indicator=0;

%removing a point based on the variance
for i=1:size(projected_value,2)
   temp_rand=temp_rand-projected_variance(i);
   if temp_rand<= fake_zero
       output=initial_matrix(:,i);
       data_remaining(i)=0;
       indicator=1;
       break;
   end
end   

%removing the last point when no point is removed. However, this is due to
%some numerical problems.
if indicator==0
    warning('numerical problem in random-removal, the last sample removed')
    for i=length(data_remaining):-1:1
        if data_remaining==1
            output=initial_matrix(:,i);
              data_remaining(i)=0;
              break;
        end
    end   
end    

end

function total_cor_matrix=calculate_correlation(observation_no,initial_matrix,data_remaining)
% This function calculate the unnormalized correlation matrix for a given data_matrix.
% The output equals to correlation matrix times number of observations

%global initial_matrix;
%global data_remaining;

if sum(data_remaining)~= observation_no
     display(observation_no);
      error('the number of observations is not consistent with the columns of data_matrix');
end
if observation_no==size(initial_matrix, 2)
    total_cor_matrix=initial_matrix*initial_matrix';
else
    total_cor_matrix=initial_matrix*diag(data_remaining)*initial_matrix';
end    

end