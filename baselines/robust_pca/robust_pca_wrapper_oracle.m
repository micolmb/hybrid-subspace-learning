% Function to select the best lambda for robust PCA by maximizing the fit
% of the true low-rank subspace
%
% Inputs:
%   X: observed data matrix
%   k: true low-rank dimension
%   L_true: true low-rank component
%
% Outputs:
%   bestL: best estimate of low-rank component
%   bestS: best estimate of sparse component
%   bestErr: best low-rank subspace error

function [bestL,bestS,bestErr] = robust_pca_wrapper_oracle(X,k,L_true)

    % set candidate lambda values
    defaultLambda = 1/sqrt(length(X));
    lambdaVals = defaultLambda*[2.^(-5:-4) 2.^(-3:0.25:3)];
    
    % initialize best values
    bestErr = Inf;
    bestL = [];
    bestS = [];
    
    % run robust PCA for multiple values of lambda
    %errors1 = zeros(length(lambdaVals),1);
    %errors2 = zeros(length(lambdaVals),1);
    %errors3 = zeros(length(lambdaVals),1);
    %errors4 = zeros(length(lambdaVals),1);
    %ind = sum(abs(L_true)) ~= 0;
    for i = 1:length(lambdaVals)
        lambda = lambdaVals(i);
        [L,S] = robustpca(X,lambda);
        err = calc_subspace_err(L_true,L,k);
        if err < bestErr
            bestErr = err;
            bestL = L;
            bestS = S;
        end
        %errors1(i) = calc_subspace_err(L_true,L,k);
        %errors2(i) = calc_subspace_err(L_true(:,ind),L(:,ind),k);
        %errors3(i) = calc_recovery_err(L_true,L);
        %errors4(i) = calc_recovery_err(L_true(:,ind),L(:,ind));
    end
    
    %figure; hold on;
    %plot(errors1,'bo-');
    %plot(errors2,'ro-');
    %plot(errors3,'co-');
    %plot(errors4,'mo-');
    %legend({'Recovery of row space from full L','Recovery of row space from reduced L',...
    %    'Exact recovery of full L','Exact recovery of reduced L'},'FontSize',13);
    %title('Robust PCA','FontSize',14);
    
end