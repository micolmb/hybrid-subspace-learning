% Function to select the best lambda for sparse PCA by maximizing the fit
% of the true low-rank subspace
%
% Inputs:
%   X: observed data matrix
%   k: true low-rank dimension
%   L_true: true low-rank component
%
% Outputs:
%   bestL: best estimate of low-rank component
%   bestAp: best estimate of sparse low-rank coefficients
%   bestErr: best low-rank subspace error

function [bestL,bestAp,bestErr] = sparse_pca_wrapper_oracle(X,k,L_true)

    % set candidate lambda values
    lambdaVals = [0 2.^(-15:-1)];
    
    % initialize best values
    bestErr = Inf;
    bestL = [];
    bestAp = [];
    
    % run sparse PCA for multiple values of lambda
    %errors1 = zeros(length(lambdaVals),1);
    %errors2 = zeros(length(lambdaVals),1);
    %errors3 = zeros(length(lambdaVals),1);
    %errors4 = zeros(length(lambdaVals),1);
    %ind = sum(abs(L_true)) ~= 0;
    for i = 1:length(lambdaVals)
        lambda = lambdaVals(i);
        colNorms = sqrt(sum(X.^2));
        Xstd = X./repmat(colNorms,size(X,1),1);
        Ap = spca(Xstd,[],k,Inf,lambda);
        Ap(isnan(Ap) | isinf(Ap)) = 0;
        A = pinv(Ap);
        Z = Xstd*Ap;
        L = (Z*A).*repmat(colNorms,size(X,1),1);
        err = calc_subspace_err(L_true,L,k);
        if err < bestErr
            bestErr = err;
            bestL = L;
            bestAp = Ap;
        end
        %errors1(i) = calc_subspace_err(L_true,L,k);
        %errors2(i) = calc_subspace_err(L_true(:,ind),L(:,ind),k);
        %errors3(i) = calc_recovery_err(L_true,L);
        %errors4(i) = calc_recovery_err(L_true(:,ind),L(:,ind));
    end
    
    %figure; hold on;
    %plot(errors1,'bo-');
    %plot(errors2,'ro-');
    %plot(errors3,'co-');
    %plot(errors4,'mo-');
    %legend({'Recovery of row space from full L v1','Recovery of row space from full L v2','Exact recovery of L'},'FontSize',13);
    %legend({'Recovery of row space from full L','Recovery of row space from reduced L',...
    %    'Exact recovery of full L','Exact recovery of reduced L'},'FontSize',13);
    %title('Sparse PCA','FontSize',14);
    
end