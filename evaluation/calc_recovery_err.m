% Function to calculate the reconstruction error between X_true and X_hat
%
% Inputs:
%   X_true: the true X
%   X_hat: the estimated X
%
% Outputs:
%   err: reconstruction error

function err = calc_recovery_err(X_true,X_hat)

    err = norm(X_true-X_hat,'fro')/sqrt(numel(X_true));
    
end