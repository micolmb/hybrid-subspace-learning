% Function to calculate the precision, recall, and f1 score of the feature
% assignment in f_hat relative to f_true
%
% Inputs:
%   f_true: the true assignment of each feature
%   f_hat: the estimated assignment of each feature
%
% Outputs:
%   prec: precision of feature assignment
%   recall: recall of feature assignment
%   f1: f1 score of feature assignment

function [prec,rec,f1] = calc_assignment_err(f_true,f_hat)

    tp = sum(f_true & f_hat);
    fp = sum(~f_true & f_hat);
    fn = sum(f_true & ~f_hat);
    
    prec = tp / (tp + fp);
    if (tp + fp == 0)
        prec = 1;
    end
    
    rec = tp / (tp + fn);
    if (tp + fn == 0)
        rec = 0;
    end
    
    f1 = 2*prec*rec / (prec + rec);

end