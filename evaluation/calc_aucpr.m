% Function to calculate area under the precision-recall curve
%
% Inputs:
%   prec: precision values at each point in the curve
%   rec: recall values at each point in the curve
%
% Outputs:
%   aucpr: area under the curve

function aucpr = calc_aucpr(prec,rec)

    % initialize area
    aucpr = 0;
    
    % add upper left corner
    if sum(rec == 0) == 0
        rec = [rec ; 0];
        prec = [prec ; 1];
    end
    
    % add bottom right corner
    if sum(rec == 1) == 0
        rec = [1 ; rec];
        prec = [0 ; prec];
    end
    
    % calculate area
    recVals = sort(unique(rec));
    for i = 1:length(recVals)-1
        currRec = recVals(i);
        nextRec = recVals(i+1);
        currPrec = prec(rec==currRec);
        nextPrec = prec(rec==nextRec);
        area = 0.5*(min(currPrec)+max(nextPrec))*(nextRec-currRec);
        aucpr = aucpr + area;
    end

end