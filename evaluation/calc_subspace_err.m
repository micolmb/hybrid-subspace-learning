% Function to calculate the error between the subspace spanned by the 
% columns of X_true and the one spanned by the columns of X_hat
%
% Inputs:
%   X_true: the true X
%   X_hat: the estimated X
%   k: dimension of the subspace
%
% Outputs:
%   err: frobenius norm error

function err = calc_subspace_err(X_true,X_hat,k)

    [~,~,V_true] = svds(X_true,k);
    VV_true = V_true*V_true';
    
    [~,~,V_hat] = svds(X_hat,k);
    VV_hat = V_hat*V_hat';
    
    err = norm(VV_true-VV_hat','fro');
    
end