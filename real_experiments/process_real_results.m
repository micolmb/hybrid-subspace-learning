% Function to refit real data results to training and test sets

function process_real_results(dataDir,cancerType,dataType,resultsDir,k,lambda)

    % load data
    X = load(fullfile(dataDir,sprintf('%s_%s_data_file.txt',cancerType,dataType)));

    % remove features that have >= 1% missing data
    numMissing = sum(isnan(X));
    keepInd = numMissing < size(X,1)*0.01;
    X = X(:,keepInd);

    % replace other missing values with column mean
    colMeans = repmat(nanmean(X),size(X,1),1);
    X(isnan(X)) = colMeans(isnan(X));
    
    % load training and test indices
    load(fullfile(resultsDir,sprintf('%s_%s_train_test_ind.mat',cancerType,dataType)));

    % split data into training and test sets
    Xtrain = X(trainInd,:);
    Xtest = X(testInd,:);

    % mean center the data
    mu = mean(Xtrain);
    Xtrain = Xtrain - repmat(mu,size(Xtrain,1),1);
    Xtest = Xtest - repmat(mu,size(Xtest,1),1);

    % normalize the data
    nrm = max(norm(Xtrain,'fro')./sqrt(numel(Xtrain)),1);
    Xtrain = Xtrain./nrm;
    Xtest = Xtest./nrm;
    
    % load HSL results
    %r = load(fullfile(resultsDir,sprintf('%s_%s_hsl_raw_results_k%d_lambda%g.mat',cancerType,dataType,k,lambda)));
    %A = r.A;
    %b = r.b;
    
    % refit data to training and test sets
    %option = struct('innertol',1e-4,'outertol',1e-3);
    %[Ztrain,Wtrain] = hybrid_sl_refit(Xtrain,A,b,k,[],[],option);
    %[Ztest,Wtest] = hybrid_sl_refit(Xtest,A,b,k,[],[],option);
    
    % save HSL results
    %resultsFile = sprintf('%s_%s_hsl_results_k%d_lambda%g.mat',cancerType,dataType,k,lambda);
    %save(fullfile(resultsDir,resultsFile),'A','b','Ztrain','Wtrain','Ztest','Wtest','mu','nrm');
    
    % evaluate HSL reconstruction error
    %XtrainHat = Ztrain*A + Wtrain*diag(b);
    %XtestHat = Ztest*A + Wtest*diag(b);
    %errTrainHSL = sum(sum((Xtrain - XtrainHat).^2))/sqrt(numel(Xtrain));
    %errTestHSL = sum(sum((Xtest - XtestHat).^2))/sqrt(numel(Xtest));
    
    % clear HSL results
    %clear A b Ztrain Wtrain Ztest Wtest XtrainHat XtestHat
    
    keyboard;
    % run PCA on the data
    %[coeff,score] = pca(Xtrain,'Centered',false,'NumComponents',k); 
    %A = transpose(coeff);
    %Ztrain = score;
    %Ztest = Xtest*pinv(A);
    
    % save PCA results
    %resultsFile = sprintf('%s_%s_pca_results_k%d.mat',cancerType,dataType,k);
    %save(fullfile(resultsDir,resultsFile),'A','Ztrain','Ztest','mu','nrm');
    
    % evaluate PCA reconstruction error
    %XtrainHat = Ztrain*A;
    %XtestHat = Ztest*A;
    %errTrainPCA = sum(sum((Xtrain - XtrainHat).^2))/sqrt(numel(Xtrain));
    %errTestPCA = sum(sum((Xtest - XtestHat).^2))/sqrt(numel(Xtest));
    
    % clear PCA results
    % clear A Ztrain Ztest XtrainHat XtestHat
    
    % run Robust PCA (how to choose lambda?)
    %lambda = 1/sqrt(max(size(Xtrain)));
    %[Ltrain,Strain] = robustpca(Xtrain,lambda);
    
    % save Robust PCA results
    %resultsFile = sprintf('%s_%s_rpca_results_k%d_lambda%g.mat',cancerType,dataType,k,round(lambda,1));
    %save(fullfile(resultsDir,resultsFile),'Ltrain','Strain','mu','nrm');
    
    % evaluate Robust PCA reconstruction error
    %XtrainHat = Ltrain + Strain;
    %errTrainRPCA = sum(sum((Xtrain - XtrainHat).^2))/sqrt(numel(Xtrain));
    
    % clear RPCA results
    % clear Ltrain Strain XtrainHat
    
    keyboard;
    % run Outlier PCA (how to choose lambda?)
    lambda = 20/sqrt(0.1*size(Xtrain,2));
    [Ltrain,Strain] = mrpca(Xtrain,ones(size(Xtrain)),lambda);
    
    % save Outlier PCA results
    resultsFile = sprintf('%s_%s_opca_results_k%d_lambda%g.mat',cancerType,dataType,k,round(lambda,1));
    save(fullfile(resultsDir,resultsFile),'Ltrain','Strain','mu','nrm');
    
    % evaluate Outlier PCA reconstruction error
    XtrainHat = Ltrain + Strain;
    errTrainOPCA = sum(sum((Xtrain - XtrainHat).^2))/sqrt(numel(Xtrain));
    
    % clear OPCA results
    % clear Ltrain Strain XtrainHat
    
    % print reconstruction errors
    fprintf('Reconstruction error on Xtrain...\n');
    fprintf('\t Hybrid SL: %g',errTrainHSL);
    fprintf('\t PCA: %g',errTrainPCA);
    fprintf('\t Robust PCA: %g',errTrainRPCA);
    fprintf('\t Outlier PCA: %g',errTrainOPCA);
    fprintf('Reconstruction error on Xtest...\n');
    fprintf('\t Hybrid SL: %g',errTestHSL);
    fprintf('\t PCA: %g',errTestPCA);
    
end