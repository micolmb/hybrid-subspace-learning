% Function to run hybrid subspace learning on BRCA control data
%
% Inputs:
%   dataDir: directory where input data is stored
%   outputDir: directory where results should be saved
%   dataType: should be Expression or Methyl

function run_brca_control_experiment(dataDir,outputDir,dataType)

    % load BRCA gene expression data
    X = load(fullfile(dataDir,sprintf('brca_control_%s_data_file.txt',dataType)));

    % remove features that have >= 1% missing data
    numMissing = sum(isnan(X));
    keepInd = numMissing < size(X,1)*0.01;
    X = X(:,keepInd);

    % replace other missing values with column mean
    colMeans = repmat(nanmean(X),size(X,1),1);
    X(isnan(X)) = colMeans(isnan(X));

    % mean center the data
    colMeans = mean(X);
    X = X - repmat(colMeans,size(X,1),1);

    % project data to l2 ball
    %X = l2_project(X,2);
    matNrm = norm(X,'fro')./sqrt(numel(X));
    X = X./max(matNrm,1);

    % set candidate values of k and lambda
    %kVals = [5 10 20 30];
    kVals = [15 30];
    lambdaMax = max(sum(abs(X)));
    lambdaVals = lambdaMax*[0 2.^(-8:-7) 2.^(-6:0.5:-1)];

    % perform parameter selection
    option1 = struct('verbose',1,'save',1,'outputDir',outputDir);
    option2 = struct('verbose',1,'innertol',1e-3,'outertol',1e-2);
    hybrid_sl_select_params(X,kVals,lambdaVals,option1,option2);

end