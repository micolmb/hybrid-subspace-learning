
% set directories
dataDir = '~/Dropbox/hybrid_subspace/tcga_data/preprocessed';
resultsDir = '~/Dropbox/hybrid_subspace';

% set cancer and data types
cancerType = 'gbm'; % one of brca, gbm, kirc
dataType = 'Expression'; % one of Expression, Methyl

% set k and lambda
k = 10; % 30 for brca, 25 for gbm, 20 for kirc
lambda = 278.3; % 157 for brca, 135 for gbm, 7 for kirc

% load raw data
X = load(fullfile(dataDir,sprintf('%s_%s_data_file.txt',cancerType,dataType)));

% remove missing values
numMissing = sum(isnan(X));
keepInd = numMissing < size(X,1)*0.01;
X = X(:,keepInd);
colMeans = repmat(nanmean(X),size(X,1),1);
X(isnan(X)) = colMeans(isnan(X));

% get training and test sets
load(fullfile(resultsDir,sprintf('%s_train_test_ind.mat',cancerType)));
Xtrain = X(trainInd,:);
Xtest = X(testInd,:);

% mean center data
mu = mean(Xtrain);
XtrainCtr = Xtrain - repmat(mu,size(Xtrain,1),1);
XtestCtr = Xtest - repmat(mu,size(Xtest,1),1);

% run PCA on training set, fit to test set
[coeff,score] = pca(Xtrain,'Centered',true,'NumComponents',k); 
Ztrain_pca = score;
A_pca = transpose(coeff);
Ztest_pca = XtestCtr*pinv(A_pca);

% measure reconstruction error for PCA
XtrainHat_pca = Ztrain_pca*A_pca;
XtestHat_pca = Ztest_pca*A_pca;
errTrainPCA = sum(sum((XtrainCtr - XtrainHat_pca).^2));
errTestPCA = sum(sum((XtestCtr - XtestHat_pca).^2));

% load results of HSL
r = load(fullfile(resultsDir,sprintf('%s_%s_results_k%d_lambda%d.mat',cancerType,dataType,k,lambda)));
if ~exist('r.Ztrain','var') || ~exist('r.Ztest','var')
    %XtrainProj = l2_project(XtrainCtr,2);
    %XtestProj = l2_project(XtestCtr,2);
    matNrm = norm(XtrainCtr,'fro')./sqrt(numel(XtrainCtr));
    XtrainProj = XtrainCtr./max(matNrm,1);
    XtestProj = XtestCtr./max(matNrm,1);
    [r.Ztrain,r.Wtrain] = hybrid_sl_refit(XtrainProj,r.A,r.b,size(r.A,1));
    [r.Ztest,r.Wtest] = hybrid_sl_refit(XtestProj,r.A,r.b,size(r.A,1));
end
Ztrain_hsl = r.Ztrain;
Ztest_hsl = r.Ztest;
A_hsl = r.A;
Wtrain_hsl = r.Wtrain;
Wtest_hsl = r.Wtest;
b_hsl = r.b;

% measure reconstruction error for HSL
%trainNrm = sqrt(sum(XtrainCtr.^2,2)./size(Xtrain,2));
trainNrm = norm(XtrainCtr,'fro')/sqrt(numel(XtrainCtr));
trainRescale = max(trainNrm,1);
XtrainHat_hsl = (Ztrain_hsl*A_hsl + Wtrain_hsl*diag(b_hsl)).*trainRescale; %repmat(trainRescale,1,size(Xtrain,2));
%testNrm = sqrt(sum(XtestCtr.^2,2)./size(Xtest,2));
testNrm = norm(XtestCtr,'fro')/sqrt(numel(XtestCtr));
testRescale = max(testNrm,1);
XtestHat_hsl = (Ztest_hsl*A_hsl + Wtest_hsl*diag(b_hsl)).*testRescale; %repmat(testRescale,1,size(Xtest,2));
errTrainHSL = sum(sum((XtrainCtr - XtrainHat_hsl).^2));
errTestHSL = sum(sum((XtestCtr - XtestHat_hsl).^2));
