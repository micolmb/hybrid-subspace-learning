% Function to run hybrid subspace learning on KIRC cancer data
%
% Inputs:
%   dataDir: directory where input data is stored
%   outputDir: directory where results should be saved
%   dataType: should be Expression or Methyl

function run_kirc_experiment(dataDir,outputDir,dataType)

    % load KIRC gene expression data
    X = load(fullfile(dataDir,sprintf('kirc_%s_data_file.txt',dataType)));

    % remove features that have >= 1% missing data
    numMissing = sum(isnan(X));
    keepInd = numMissing < size(X,1)*0.01;
    X = X(:,keepInd);

    % replace other missing values with column mean
    colMeans = repmat(nanmean(X),size(X,1),1);
    X(isnan(X)) = colMeans(isnan(X));

    % split data into training and test sets
    n = size(X,1);
    ntrain = round(n*0.8);
    ntest = n - ntrain;
    trainInd = randsample(1:n,ntrain);
    testInd = setdiff(1:n,trainInd);
    Xtrain = X(trainInd,:);
    Xtest = X(testInd,:);

    % mean center the data
    colMeans = mean(Xtrain);
    Xtrain = Xtrain - repmat(colMeans,ntrain,1);
    Xtest = Xtest - repmat(colMeans,ntest,1);

    % project data to l2 ball
    %Xtrain = l2_project(Xtrain,2);
    %Xtest = l2_project(Xtest,2);
    matNrm = norm(Xtrain,'fro')./sqrt(numel(Xtrain));
    Xtrain = Xtrain./max(matNrm,1);
    Xtest = Xtest./max(matNrm,1);
    
    % set candidate values of k and lambda
    %kVals = [10 20 30 40 80];
    kVals = [10 15 20];
    lambdaMax = max(sum(abs(Xtrain)));
    lambdaVals = lambdaMax*[0 2.^(-8:-7) 2.^(-6:0.5:-1)];

    % save training and test indices
    save(fullfile(outputDir,'train_test_ind.mat'),'trainInd','testInd');

    % perform parameter selection
    option1 = struct('verbose',1,'save',1,'outputDir',outputDir);
    option2 = struct('verbose',1,'innertol',1e-3,'outertol',1e-2);
    hybrid_sl_select_params(Xtrain,kVals,lambdaVals,option1,option2);

end