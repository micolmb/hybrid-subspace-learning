#include <math.h>
#include "mex.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* macros for arguments */
    #define X_IN prhs[0]
    #define d_IN prhs[1]
    #define P_OUT plhs[0]

    /* error check number of inputs */
    if (nrhs < 2) {
        mexErrMsgTxt("Too few input arguments.");
    }
    else if (nrhs > 2) {
        mexErrMsgTxt("Too many input arguments.");
    }
    
    /* error check number of outputs */
    if (nlhs < 1) {
        mexErrMsgTxt("Too few output arguments.");
    } else if (nlhs > 1) {
        mexErrMsgTxt("Too many input arguments.");    
    }
    
    /* declare counters */
    int row;
    int col;
    
    /* process X */
    double *X = mxGetPr(X_IN);
    int nRowX = mxGetM(X_IN);
    int nColX = mxGetN(X_IN);
    
    /* process d */
    double *d = mxGetPr(d_IN);
    double dim = d[0];    
    
    /* create P */
    P_OUT = mxCreateDoubleMatrix(nRowX,nColX,mxREAL);
    double *P = mxGetPr(P_OUT);
    
    /* prox operator */
    if (dim == 1) {
        for (row = 0; row < nRowX; row++) {
            double nrm = 0;
            for (col = 0; col < nColX; col++) {
                nrm += X[row+nRowX*col]*X[row+nRowX*col];
            }
            nrm = sqrt(nrm/(double) nColX);
            if (nrm <= 1) {
                for (col = 0; col < nColX; col++) {                    
                    P[row+nRowX*col] = X[row+nRowX*col];
                }
            }
            else {
                for (col = 0; col < nColX; col++) {
                    P[row+nRowX*col] = X[row+nRowX*col]/nrm;
                }
            }           
        }
    }
    else if (dim == 2) {
        for (col = 0; col < nColX; col++) {
            double nrm = 0;
            for (row = 0; row < nRowX; row++) {
                nrm += X[row+nRowX*col]*X[row+nRowX*col];
            }
            nrm = sqrt(nrm/(double) nRowX);
            if (nrm <= 1) {
                for (row = 0; row < nRowX; row++) {                    
                    P[row+nRowX*col] = X[row+nRowX*col];
                }
            }
            else {
                for (row = 0; row < nRowX; row++) {
                    P[row+nRowX*col] = X[row+nRowX*col]/nrm;
                }
            }   
        }
    }
    else {
        mexErrMsgTxt("dim must be either 1 (row-wise) or 2 (column-wise)");
    }
    
    return;
}