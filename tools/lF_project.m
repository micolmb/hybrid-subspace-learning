% Frobenius Norm Ball Projection Operator
%
% Inputs:
%   X: input to projection operator - can be a scalar, vector, or matrix
% 
% Outputs:
%   P: output of projection operator - same size as X

function P = lF_project(X)

    nrm = norm(X,'fro')./sqrt(numel(X));
    P = X./max(nrm,1);

end