% L2 Ball Projection Operator
%
% Inputs:
%   X: input to projection operator - can be a scalar, vector, or matrix
%   dim: dimension along which to operate, if X is a matrix
% 
% Outputs:
%   P: output of projection operator - same size as X

function P = l2_project(X,dim)

    if min(size(X)) == 1 % scalar or vector
        n = length(X);
        nrm = sqrt(sum(X.^2)./n);
        P = X/max(nrm,1);
    else % matrix
        n = size(X,dim);
        nrms = sqrt(sum(X.^2,dim)./n);
        divs = max(nrms,1);
        P = bsxfun(@rdivide,X,divs);
    end

end