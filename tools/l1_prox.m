% L1 Norm Prox Operator
%
% Inputs:
%   X: input to prox operator - can be a scalar, vector, or matrix
%   u: threshold value(s) - must be a scalar or the same size as X
% 
% Outputs:
%   P: output of prox operator - same size as X

function P = l1_prox(X,u)

    P = sign(X).*max(0,abs(X)-u);

end
