#include <math.h>
#include "mex.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* macros for arguments */
    #define X_IN prhs[0]
    #define W_IN prhs[1]
    #define d_IN prhs[2]
    #define P_OUT plhs[0]
            
    /* error check number of inputs */
    if (nrhs < 3) {
        mexErrMsgTxt("Too few input arguments.");
    }
    else if (nrhs > 3) {
        mexErrMsgTxt("Too many input arguments.");
    }
    
    /* error check number of outputs */
    if (nlhs < 1) {
        mexErrMsgTxt("Too few output arguments.");
    }
    else if (nlhs > 1) {
        mexErrMsgTxt("Too many input arguments.");    
    }
    
    /* declare counters */
    int row;
    int col;
    
    /* process X */
    double *X = mxGetPr(X_IN);
    int nRowX = mxGetM(X_IN);
    int nColX = mxGetN(X_IN);
    
    /* process d */
    double *d = mxGetPr(d_IN);
    double dim = d[0];    
    
    /* process U */
    double *W = mxGetPr(W_IN);
    int nRowW = mxGetM(W_IN);
    int nColW = mxGetN(W_IN);
    if (nRowW > 1 && nColW > 1) {
        mexErrMsgTxt("W must be a vector.");
    }
    int nElemW = fmax(nRowW,nColW);
    if (dim == 1 && nElemW != nRowX) {
        mexErrMsgTxt("W must have the same number of elements as rows of X");
    }
    else if (dim == 2 && nElemW != nColX) {
        mexErrMsgTxt("W must have the same number of elements as columns of X");
    }

    /* create P */
    P_OUT = mxCreateDoubleMatrix(nRowX,nColX,mxREAL);
    double *P = mxGetPr(P_OUT);
    
    /* prox operator */
    if (dim == 1) {
        for (row = 0; row < nRowX; row++) {
            double nrm = 0;
            for (col = 0; col < nColX; col++) {
                nrm += X[row+nRowX*col]*X[row+nRowX*col];
            }
            nrm = sqrt(nrm);
            if (nrm == 0) continue;
            double w;
            if (nRowW == 1 && nColW == 1) {
                w = W[0];
            }
            else {
                w = W[row];
            }
            double mult = fmax(0,nrm-w)/nrm;
            for (col = 0; col < nColX; col++) {
                P[row+nRowX*col] = X[row+nRowX*col]*mult;
            }
        }
    }
    else if (dim == 2) {
        for (col = 0; col < nColX; col++) {
            double nrm = 0;
            for (row = 0; row < nRowX; row++) {
                nrm += X[row+nRowX*col]*X[row+nRowX*col];
            }
            nrm = sqrt(nrm);
            if (nrm == 0) continue;
            double w;
            if (nRowW == 1 && nColW == 1) {
                w = W[0];
            }
            else {
                w = W[col];
            }
            double mult = fmax(0,nrm-w)/nrm;
            for (row = 0; row < nRowX; row++) {
                P[row+nRowX*col] = X[row+nRowX*col]*mult;
            }
        }
    }
    else {
        mexErrMsgTxt("dim must be either 1 (row-wise) or 2 (column-wise)");
    }
    
    return;
}