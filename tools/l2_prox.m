% L2 Norm Prox Operator
%
% Inputs:
%   X: input to prox operator - can be a scalar, vector, or matrix
%   u: threshold value(s) - must be a scalar or a vector w/ the same number
%      of columns (if dim = 1) or rows (if dim = 2) as X
%   dim: dimension along which to operate, if X is a matrix
% 
% Outputs:
%   P: output of prox operator - same size as X

function P = l2_prox(X,u,dim)

    if max(size(X)) == 1 % scalar or vector
        nrm = sqrt(sum(X.^2));
        nrm(nrm==0) = 1;
        P = X.*max(0,nrm-u)./nrm;
    else % matrix
        nrms = sqrt(sum(X.^2,dim));
        divs = nrms;
        divs(divs==0) = 1;        
        P = bsxfun(@times,X,max(0,nrms-u)./divs);
    end

end