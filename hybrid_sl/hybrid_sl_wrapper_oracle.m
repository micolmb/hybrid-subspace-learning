% Function to select the best lambda for hybrid SL by maximizing the fit
% of the true low-rank subspace
%
% Inputs:
%   X: observed data matrix
%   k: true low-rank dimension
%   L_true: true low-rank component
%
% Outputs:
%   bestZ: best estimate of low-rank features
%   bestA: best estimate of low-rank coefficients
%   bestW: best estimate of high-dim features
%   bestb: best estimate of high-dim coefficients
%   bestErr: best low-rank subspace error

function [bestZ,bestA,bestW,bestb,bestErr] = hybrid_sl_wrapper_oracle(X,k,L_true,S_true)

    % set optimization options
    option = struct('innertol',1e-5,'outertol',1e-3);
    
    % set candidate lambda values
    lambdaMax = max(sum(abs(X)));
    lambdaVals = [0 lambdaMax.*(2.^(-15:0))];
    
    % initialize best values
    bestErr = Inf;
    bestZ = [];
    bestA = [];
    bestW = [];
    bestb = [];
    
    % non-overlapping case
    if sum(sum(abs(L_true)).*sum(abs(S_true))) == 0
        
        % run hybrid SL for multiple values of lambda
        %errors1 = zeros(length(lambdaVals),1);
        %errors2 = zeros(length(lambdaVals),1);
        %errors3 = zeros(length(lambdaVals),1);
        %errors4 = zeros(length(lambdaVals),1);
        %ind = sum(abs(L_true)) ~= 0;
        for i = 1:length(lambdaVals)
            lambda = lambdaVals(i);
            [Z,A,W,b] = hybrid_sl_wrapper_exclusive(X,k,lambda,option);
            err = calc_subspace_err(L_true,Z*A,k);
            if err < bestErr
                bestErr = err;
                bestZ = Z;
                bestA = A;
                bestW = W;
                bestb = b;
            end
            %L = Z*A;
            %errors1(i) = calc_subspace_err(L_true,L,k);
            %errors2(i) = calc_subspace_err(L_true(:,ind),L(:,ind),k);
            %errors3(i) = calc_recovery_err(L_true,L);
            %errors4(i) = calc_recovery_err(L_true(:,ind),L(:,ind));
        end

        %figure; hold on;
        %plot(errors1,'bo-');
        %plot(errors2,'ro-');
        %plot(errors3,'co-');
        %plot(errors4,'mo-');
        %legend({'Recovery of row space from full L v1','Recovery of row space from full L v2','Exact recovery of L'},'FontSize',13);
        %legend({'Recovery of row space from full L','Recovery of row space from reduced L',...
        %    'Exact recovery of full L','Exact recovery of reduced L'},'FontSize',13);
        %title('Hybrid SL','FontSize',14);
    
    % overlapping case
    else
        
        for i = 1:length(lambdaVals)
            lambda = lambdaVals(i);
            gamma = 0;
            Z = [];
            A = [];
            W = [];
            b = [];
            while (true)
                [Z,A,W,b] = hybrid_sl_optimization_accel(X,k,gamma,0,lambda,Z,A,W,b,option);
                err = calc_subspace_err(L_true,Z*A,k);
                if err < bestErr
                    bestErr = err;
                    bestZ = Z;
                    bestA = A;
                    bestW = W;
                    bestb = b;
                end
                if sum(sum(abs(A)).*abs(b')) == 0
                    break;
                elseif gamma == 0
                    gamma = 0.25;
                else
                    gamma = 2*gamma;
                end
            end
        end
        
    end
    
end