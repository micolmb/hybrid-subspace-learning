% Function to perform parameter selection for hybrid low-rank + sparse
% subspace learning. Chooses k and lambda that minimize AIC score.
%
% Inputs:
%   X: observed data, n-by-p matrix of observed features
%   kVals: vector of candidate values for k
%   lambdaVals: vector of candidate values for lambda (optional)
%   selOption: struct of parameter selection options (optional)
%   optOption: struct of optimization parameters (optional)
%
% Outputs:
%   Z: n-by-k matrix of low-rank component features
%   A: k-by-p matrix of low-rank component coefficients
%   W: n-by-p matrix of high-dim component features
%   b: p-by-1 vector of high-dim component coefficients
%   k: selected value of k
%   lambda: selected value of lambda
%   results: results struct containing loss value, sparsity, 
%            and aic score for each parameter setting

function [Z,A,W,b,k,lambda,results] = hybrid_sl_select_params(X,kVals,lambdaVals,selOption,optOption)

    % set default options for variable selection
    if ~exist('selOption','var'); selOption = []; end;
    if ~isfield(selOption,'gamma'); selOption.gamma = 'max'; end
    if ~isfield(selOption,'verbose'); selOption.verbose = 0; end;
    if ~isfield(selOption,'save'); selOption.save = 0; end;

    % set default options for optimization    
    if ~exist('optOption','var'); optOption = []; end;

    % set default lambda values
    if ~exist('lambdaVals','var') || isempty(lambdaVals)
        lambdaMax = max(sum(abs(X)));
        lambdaVals = lambdaMax.*(2.^(-15:0));
    end

    % initialize storage for loss, sparsity, AIC
    lossVal = zeros(length(kVals),length(lambdaVals));
    lrFrac = zeros(length(kVals),length(lambdaVals));
    hdFrac = zeros(length(kVals),length(lambdaVals));
    aicScore = zeros(length(kVals),length(lambdaVals));
    
    % initialize best variables
    bestAic = Inf;
    bestZ = [];
    bestA = [];
    bestW = [];
    bestb = [];

    % evaluate each k and lambda
    for kInd = 1:length(kVals)
        k = kVals(kInd);
        for lambdaInd = 1:length(lambdaVals)
            lambda = lambdaVals(lambdaInd);
            % print status
            if selOption.verbose
                fprintf('Running with k = %d, lambda = %g...\n',k,lambda);
            end
            % choose maximum lambda, fit model to data
            if strcmp(selOption.gamma,'max')
                [Z,A,W,b,objVals,gammaVals,aicVals] = hybrid_sl_wrapper_exclusive(X,k,lambda,optOption);
            % choose best lambda, fit model to data
            elseif strcmp(selOption.gamma,'best')
                [Z,A,W,b,objVals,gammaVals,aicVals] = hybrid_sl_wrapper_overlap(X,k,lambda,optOption);
            end
            % measure loss value
            lossVal(kInd,lambdaInd) = sum(sum((X - Z*A - W*diag(b)).^2))/2;
            % measure low-rank/high-dim proportions
            lrFrac(kInd,lambdaInd) = mean(sum(abs(A)) ~= 0);
            hdFrac(kInd,lambdaInd) = mean(abs(b) ~= 0);
            % calculate AIC score
            p1 = sum(sum(abs(A)) ~= 0);
            p2 = sum(abs(b) ~= 0);
            numParam = k*(k-1)/2 + k*p1 + 1 + p2*(p2-1)/2 + p2;
            aicScore(kInd,lambdaInd) = 2*numParam + 2*lossVal(kInd,lambdaInd);
            % print status
            if selOption.verbose
                fprintf('Result: loss = %g, aic = %g, low-rank/sparse = %.2f/%.2f\n',...
                    lossVal(kInd,lambdaInd),aicScore(kInd,lambdaInd),lrFrac(kInd,lambdaInd),hdFrac(kInd,lambdaInd));
            end
            % store results
            if aicScore(kInd,lambdaInd) <= bestAic
                bestZ = Z;
                bestA = A;
                bestW = W;
                bestb = b;
                bestAic = aicScore(kInd,lambdaInd);
            end
            % save results
            if selOption.save
                outputFile = fullfile(selOption.outputDir,sprintf('results_k%d_lambda%g.mat',k,round(lambda*10)/10));
                save(outputFile,'A','b','objVals','gammaVals','aicVals');
                outputFile = fullfile(selOption.outputDir,sprintf('results_summary.mat'));
                save(outputFile,'lossVal','lrFrac','hdFrac','aicScore');
            end
        end
    end

    % select best parameters
    [~,ind] = min(aicScore(:));
    [kInd,lambdaInd] = ind2sub(size(aicScore),ind);
    k = kVals(kInd);
    lambda = lambdaVals(lambdaInd);
    
    % print best parameters
    if selOption.verbose
        fprintf('Best k = %d, Best lambda = %g\n',k,lambda);
    end
    
    % return best variables
    Z = bestZ;
    A = bestA;
    W = bestW;
    b = bestb;

    % save overall results
    if selOption.save
        outputFile = fullfile(selOption.outputDir,sprintf('results_summary.mat'));
        save(outputFile,'lossVal','lrFrac','hdFrac','aicScore');
    end

    % return results
    results = struct('lossVal',lossVal,'lrFrac',lrFrac,'hdFrac',hdFrac,'aicScore',aicScore);

end