% Function to optimize the objective function for hybrid low-rank + sparse 
% subspace learning using alternating proximal gradient descent.
% Specifically, the problem is given by
%
%   min_{Z,A,W,b} (1/2) * ||X - Z*A - W*diag(b)||_F^2 
%       + gamma * ||A*diag(b)||_{1,2} 
%       + lambda1 * ||A||_{1,2} + lambda2 * ||b||_1 
%   s.t. ||Z||_F <= 1, ||W||_F <= 1 for i = 1,...,n
%
% Inputs:
%   X: n-by-p matrix of observed features
%   k: dimensionality of low-rank latent space
%   gamma: regularization parameter for joint penalty on A and b
%   lambda1: regularization parameter for individual penalty on A
%   lambda2: regularization parameter for individual penalty on b
%   initZ: initial value for Z (optional)
%   initA: initial value for A (optional)
%   initW: initial value for W (optional)
%   initb: initial value for b (optional)
%   option: struct of optimization parameters (optional)
%
% Outputs:
%   Z: n-by-k matrix of low-rank component features
%   A: k-by-p matrix of low-rank component coefficients
%   W: n-by-p matrix of high-dim component features
%   b: p-by-1 vector of high-dim component coefficients
%   objVals: objective value at each iteration
%   objIds: indicator of which subroutine was running at each iteration

function [Z,A,W,b,objVals,objIds] = hybrid_sl_optimization(X,k,gamma,lambda1,lambda2,initZ,initA,initW,initb,option)

    % set options for optimization
    if ~exist('option','var'); option = []; end;
    if ~isfield(option,'seed'); rng('shuffle'); option.seed = randi(1e4); end;
    if ~isfield(option,'verbose'); option.verbose = 0; end;
    if ~isfield(option,'veryverbose'); option.veryverbose = 0; end;
    if ~isfield(option,'outeriter'); option.outeriter = 500; end;
    if ~isfield(option,'inneriter'); option.inneriter = 5000; end;
    if ~isfield(option,'outertol'); option.outertol = 1e-4; end;
    if ~isfield(option,'innertol'); option.innertol = 1e-6; end;

    % set default inputs
    if ~exist('initZ','var'); initZ = []; end;
    if ~exist('initA','var'); initA = []; end;
    if ~exist('initW','var'); initW = []; end;
    if ~exist('initb','var'); initb = []; end;

    % get dimensions
    [n,p] = size(X);
    
    % initialize variables
    [Z,A,W,b] = init_variables(n,k,p,initZ,initA,initW,initb,option.seed);

    % initialize storage
    objVals = [];
    objIds = [];
    objValsOuter = zeros(option.outeriter,1);

    % run biconvex optimization
    for iter = 1:option.outeriter          
        
        % optimize coefficients
        [W,A,objValsWA] = optimize_WA(X,Z,A,W,b,gamma,lambda1,lambda2,option);
        objVals = [objVals;objValsWA];
        objIds = [objIds;zeros(length(objValsWA),1)];

        % optimize features
        [Z,b,objValsZb] = optimize_Zb(X,Z,A,W,b,gamma,lambda1,lambda2,option);
        objVals = [objVals;objValsZb];
        objIds = [objIds;ones(length(objValsZb),1)];

        % compute objective
        objValsOuter(iter) = (1/2)*sum(sum((X-Z*A-bsxfun(@times,W,b')).^2)) + ...
            gamma*sum(sqrt(sum((bsxfun(@times,A,b')).^2,1))) + ...
            lambda1*sum(sqrt(sum(A.^2,1))) + lambda2*sum(abs(b));        
        
        % print message
        if (option.verbose)
            dnsA = 100*sum(sum(abs(A)) > 0)/p;
            dnsB = 100*sum(abs(b) > 0)/p;
            fprintf('Outer Iter %d: Obj = %.0f, Dens of A = %.1f%%, Dens of b = %.1f%%\n',iter,objValsOuter(iter),dnsA,dnsB);
        end

        % check convergence
        if (iter >= 5 && abs(objValsOuter(iter)-objValsOuter(iter-1))/max(1,abs(objValsOuter(iter-1))) < option.outertol)
            break;
        end
        
    end

end

% Function to initialize variables
function [Z,A,W,b] = init_variables(n,k,p,initZ,initA,initW,initb,seed)

    % set seed
    rng(seed);

    % initialize Z
    if isempty(initZ)
        Z = normrnd(0,1,n,k);
        Z = lF_project(Z);
    else
        Z = initZ;
    end

    % initialize A
    if isempty(initA)
        A = rand(k,p);
    else
        A = initA;
    end

    % initialize W
    if isempty(initW)
        W = normrnd(0,1,n,p);
        W = lF_project(W);
    else
        W = initW;
    end

    % initialize b
    if isempty(initb)
        b = rand(p,1);
    else
        b = initb;
    end

end

% Function to optimize {Z,b} jointly
function [Z,b,objVals] = optimize_Zb(X,Z,A,W,b,gamma,lambda1,lambda2,option)

    % store objective values
    objVals = zeros(option.inneriter,1);
    
    % compute and store penalty
    pen2 = lambda1*sum(sqrt(sum(A.^2,1)));
    
    % compute initial loss value
    gCurr = (1/2)*sum(sum((X-Z*A-bsxfun(@times,W,b')).^2));

    % set initial step size
    alpha = 1;

    % run optimization
    for iter = 1:option.inneriter
        % compute gradients
        Zgrad = -(X-Z*A-bsxfun(@times,W,b'))*A';
        bgrad = -sum((X-Z*A-bsxfun(@times,W,b')).*W,1)';
        % compute new Z and b
        for ls_it = 1:100
            Zplus = Z-alpha*Zgrad;
            Znew = lF_project(Zplus);
            Zgengrad = (Z-Znew)./alpha;
            bplus = b-alpha*bgrad;
            bnew = l1_prox(bplus,alpha*(gamma*sqrt(sum(A.^2,1))+lambda2)');
            bgengrad = (b-bnew)./alpha;
            gNew = (1/2)*sum(sum((X-Znew*A-bsxfun(@times,W,bnew')).^2));
            if (gNew <= gCurr - alpha*sum(sum(Zgrad.*Zgengrad))- alpha*sum(bgrad.*bgengrad) ...
                    + .5*alpha*sum(sum(Zgengrad.^2)) + .5*alpha*sum(bgengrad.^2))
                Z = Znew;
                b = bnew;
                break;
            else
                alpha = .5*alpha;
            end
        end
        % store new objective & loss values
        objVals(iter) = gNew + gamma*sum(sqrt(sum((bsxfun(@times,A,b')).^2,1))) + pen2 + lambda2*sum(abs(b));
        gCurr = gNew;
        % check convergence
        if (iter >= 10 && abs(objVals(iter)-objVals(iter-1))/max(1,abs(objVals(iter-1))) < option.innertol)
            break;
        end
        % print message
        if (option.veryverbose)
            fprintf('Optimizing Zb: Inner Iter %d: Obj = %f\n',iter,objVals(iter));
        end    
    end

    % prune objective values
    objVals = objVals(1:iter);
    
end

% Function to optimize {W,A} jointly
function [W,A,objVals] = optimize_WA(X,Z,A,W,b,gamma,lambda1,lambda2,option)

    % store objective values
    objVals = zeros(option.inneriter,1);
    
    % compute and store penalty
    pen3 = lambda2*sum(abs(b));
    
    % compute initial loss value
    gCurr = (1/2)*sum(sum((X-Z*A-bsxfun(@times,W,b')).^2));

    % set initial step size
    alpha = 1;

    % run optimization
    for iter = 1:option.inneriter
        % compute gradients
        Wgrad = -bsxfun(@times,(X-Z*A-bsxfun(@times,W,b')),b');
        Agrad = -Z'*(X-Z*A-bsxfun(@times,W,b'));
        % compute new W and A
        for ls_it = 1:100
            Wplus = W-alpha*Wgrad;
            Wnew = lF_project(Wplus);
            Wgengrad = (W-Wnew)./alpha;
            Aplus = A-alpha*Agrad;
            Anew = l2_prox_mex(Aplus,alpha*(gamma*abs(b)+lambda1),2);
            Agengrad = (A-Anew)./alpha;
            gNew = (1/2)*sum(sum((X-Z*Anew-bsxfun(@times,Wnew,b')).^2));            
            if (gNew <= gCurr - alpha*sum(sum(Wgrad.*Wgengrad)) - alpha*sum(sum(Agrad.*Agengrad)) ...
                    + .5*alpha*sum(sum(Wgengrad.^2)) + .5*alpha*sum(sum(Agengrad.^2)))
                W = Wnew;
                A = Anew;
                break;
            else
                alpha = .5*alpha;
            end
        end
        % store new objective & loss values
        objVals(iter) = gNew + gamma*sum(sqrt(sum((bsxfun(@times,A,b')).^2,1))) + lambda1*sum(sqrt(sum(A.^2,1))) + pen3;
        gCurr = gNew;
        % check convergence
        if (iter >= 10 && abs(objVals(iter)-objVals(iter-1))/max(1,abs(objVals(iter-1))) < option.innertol)
            break;
        end
        % print message
        if (option.veryverbose)
            fprintf('Optimizing WA: Inner Iter %d: Obj = %f\n',iter,objVals(iter));
        end        
    end

    % prune objective values
    objVals = objVals(1:iter);

end
