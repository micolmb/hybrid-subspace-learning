% Function to refit a trained hybrid low-rank + sparse subspace learning
% model to test data using projected gradient descent. Specifically, the 
% problem is given by
%
%   min_{Z,W} (1/2) * ||X - Z*A - W*diag(b)||_F^2 
%   s.t. ||Z||_F <= 1, ||W||_F <= 1 for i = 1,...,n
%
% where A and b were previously estimated on a training set
%
% Inputs:
%   X: n-by-p matrix of observed features
%   A: k-by-p matrix of low-rank component coefficients
%   b: p-by-1 vector of high-dim component coefficients
%   k: dimensionality of low-rank latent space
%   initZ: initial value for Z (optional)
%   initW: initial value for W (optional)
%   option: struct of optimization parameters (optional)
%
% Outputs:
%   Z: n-by-k matrix of low-rank component features
%   W: n-by-p matrix of high-dim component features
%   objVals: objective value at each iteration

function [Z,W,objVals] = hybrid_sl_refit(X,A,b,k,initZ,initW,option)

    % set options for optimization
    if ~exist('option','var'); option = []; end;
    if ~isfield(option,'seed'); rng('shuffle'); option.seed = randi(1e4); end;
    if ~isfield(option,'verbose'); option.verbose = 0; end;
    if ~isfield(option,'veryverbose'); option.veryverbose = 0; end;
    if ~isfield(option,'outeriter'); option.outeriter = 50; end;
    if ~isfield(option,'inneriter'); option.inneriter = 1000; end;
    if ~isfield(option,'outertol'); option.outertol = 1e-5; end;
    if ~isfield(option,'innertol'); option.innertol = 1e-6; end;

    % set default inputs
    if ~exist('initZ','var'); initZ = []; end;
    if ~exist('initW','var'); initW = []; end;

    % get dimensions
    [n,p] = size(X);
    
    % initialize variables
    [Z,W] = init_variables(n,k,p,initZ,initW,option.seed);
    
    % run optimization
    [Z,W,objValsZW] = optimize_ZW(X,Z,A,W,b,option);
    objVals = objValsZW;
    
end

% Function to initialize variables Z, W
function [Z,W] = init_variables(n,k,p,initZ,initW,seed)

    % set seed
    rng(seed);

    % initialize Z
    if isempty(initZ)
        Z = normrnd(0,1,n,k);
        Z = lF_project(Z);
    else
        Z = initZ;
    end

    % initialize W
    if isempty(initW)
        W = normrnd(0,1,n,p);
        W = lF_project(W);
    else
        W = initW;
    end

end

% Function to optimize {Z,W} jointly
function [Z,W,objVals] = optimize_ZW(X,Z,A,W,b,option)

    % store objective values
    objVals = zeros(option.inneriter,1);
    
    % set initial step size
    alpha = 1;
    
    % initialize starting values
    Zext = Z;
    Wext = W;
    theta = 1;  
    
    % run optimization
    for iter = 1:option.inneriter
        % compute gradients
        Zgrad = -(X-Zext*A-bsxfun(@times,Wext,b'))*A';
        Wgrad = -bsxfun(@times,(X-Zext*A-bsxfun(@times,Wext,b')),b');
        % compute loss value
        gCurr = (1/2)*sum(sum((X-Zext*A-bsxfun(@times,Wext,b')).^2));
        % compute new Z and W
        for ls_it = 1:100
            Zplus = Zext-alpha*Zgrad;
            Znew = lF_project(Zplus);
            Zgengrad = (Zext-Znew)./alpha;
            Wplus = Wext-alpha*Wgrad;
            Wnew = lF_project(Wplus);
            Wgengrad = (Wext-Wnew)./alpha;
            gNew = (1/2)*sum(sum((X-Znew*A-bsxfun(@times,Wnew,b')).^2));
            if (gNew <= gCurr - alpha*sum(sum(Zgrad.*Zgengrad)) + .5*alpha*sum(sum(Zgengrad.^2)) ...
                    - alpha*sum(sum(Wgrad.*Wgengrad)) + + .5*alpha*sum(sum(Wgengrad.^2)))
                break;
            else
                alpha = .5*alpha;
            end
        end
        % compute new Zext, Wext, and theta
        thetanew = (1+sqrt(1+4*theta^2))/2;
        Zext = Znew + (Znew-Z).*(theta-1)/(thetanew);
        Wext = Wnew + (Wnew-W).*(theta-1)/(thetanew);
        % store new Z, W, and theta
        Z = Znew;
        W = Wnew;
        theta = thetanew;
        % store new objective value
        objVals(iter) = gNew;
        % check convergence
        if (iter >= 10 && abs(objVals(iter)-objVals(iter-1))/max(1,abs(objVals(iter-1))) < option.innertol)
            break;
        end
    end

    % prune objective values
    objVals = objVals(1:iter);
    
end
