% Function to optimize the objective function for mutually exclusive 
% hybrid low-rank + sparse subspace learning using alternating accelerated
% proximal gradient descent. Specifically, the problem is given by
%
%   min_{Z,A,W,b} sum (1/2) * ||X - Z*A - W*diag(b)||_F^2 
%       + C * ||A*diag(b)||_{1,2} + lambda * ||b||_1 
%   s.t. ||Z||_F <= 1, ||W||_F <= 1
%
% where C is a constant large enough that ||A*diag(b)||_{1,2} = 0
%
% Inputs:
%   X: n-by-p matrix of observed features
%   k: dimensionality of low-rank latent space
%   lambda: regularization parameter for individual penalty on b
%   option: struct of optimization parameters (optional)
%
% Outputs:
%   Z: n-by-k matrix of low-rank component features
%   A: k-by-p matrix of low-rank component coefficients
%   W: n-by-p matrix of high-dim component features
%   b: p-by-1 vector of high-dim component coefficients
%   objVals: objective value at each iteration
%   gammaVals: value of gamma at each iteration
%   aicVals: aic score for each value of gamma

function [Z,A,W,b,objVals,gammaVals,aicVals] = hybrid_sl_wrapper_exclusive(X,k,lambda,option)

    % set default options for optimization    
    if ~exist('option','var'); option = []; end;
    if ~isfield(option,'verbose'); option.verbose = 0; end
    
    % initialize gamma and variables
    gamma = 0;
    Z = [];
    A = [];
    W = [];
    b = [];
    
    % initialize storage for objective values
    objVals = [];
    gammaVals = [];
    
    % initialize storage for AIC scores
    aicVals = [];
    
    % learn a sequence of models with increasing gamma
    while (true)
    
        % run optimization with current value of gamma, initialize with previous solution
        [Z,A,W,b,currObjVals] = hybrid_sl_optimization_accel(X,k,gamma,0,lambda,Z,A,W,b,option);
        objVals = [objVals ; currObjVals];
        gammaVals = [gammaVals ; gamma*ones(length(currObjVals),1)];
        
        % calculate density of A and b
        indOnA = sum(abs(A)) > 0;
        indOnB = abs(b') > 0;
        dnsA = 100*mean(indOnA);
        dnsB = 100*mean(indOnB);
        dnsBoth = 100*mean(indOnA & indOnB);
        dnsEither = 100*mean(indOnA | indOnB);
        
        % calculate AIC score
        p1 = sum(sum(abs(A)) ~= 0);
        p2 = sum(abs(b) ~= 0);
        numParam = k*(k-1)/2 + k*p1 + 1 + p2*(p2-1)/2 + p2;
        lossVal = sum(sum((X - Z*A - W*diag(b)).^2))/2;
        aicVals = [aicVals ; 2*numParam + 2*lossVal];
        
        % print message
        if (option.verbose)
            fprintf('Gamma = %g: Dens of A = %.1f%%, Dens of b = %.1f%%, Overlap = %.1f%%, Coverage = %.1f%%\n',...
                gamma,dnsA,dnsB,dnsBoth,dnsEither);
        end
        
        % check stopping criterion
        if sum(dnsBoth) == 0
            break;
        end
        
        % update gamma
        if (gamma == 0)
            gamma = .25;
        else
            gamma = 2*gamma;
        end

    end
    
end