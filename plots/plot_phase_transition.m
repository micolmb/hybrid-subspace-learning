% Script to plot phase transition results
%
% Plots:
% (1) Phase transition of success on both identification of true subspace 
% in L and identification of features in S
% (2) Phase transition of success on just recovery of L
% (3) Phase transition of success on just recovery of S
% (4) Raw mean errors on recovery of L
% (5) Raw mean F1 scores on recovery of S

% set plotting constants
axisFont = 'AvantGarde';
axisSize = 15;
axisWeight = 'normal';
titleFont = 'AvantGarde';
titleSize = 15;
titleWeight = 'bold';

% load results
resultsFile = '~/Dropbox/hybrid_subspace/synthetic_results/results_phase_transition.mat';
load(fullfile(resultsFile));

% adjust f1 score for first column
results.f1S(:,:,1,:) = 1;

% calculate success on recovery of L
successL = results.errLsub <= 1e-2;
successL_avg = mean(successL,4);
successL_rpca = squeeze(successL_avg(1,:,:));
successL_opca = squeeze(successL_avg(2,:,:));
successL_hsl = squeeze(successL_avg(3,:,:));

% calculate success on accuracy of S
successS = results.f1S == 1;
successS_avg = mean(successS,4);
successS_rpca = squeeze(successS_avg(1,:,:));
successS_opca = squeeze(successS_avg(2,:,:));
successS_hsl = squeeze(successS_avg(3,:,:));

% calculate success on both tasks
successBoth = successL & successS;
successBoth_avg = mean(successBoth,4);
successBoth_rpca = squeeze(successBoth_avg(1,:,:));
successBoth_opca = squeeze(successBoth_avg(2,:,:));
successBoth_hsl = squeeze(successBoth_avg(3,:,:));

% calculate mean error on L
resultsL = mean(results.errLsub,4);
resultsL_rpca = squeeze(resultsL(1,:,:));
resultsL_opca = squeeze(resultsL(2,:,:));
resultsL_hsl = squeeze(resultsL(3,:,:));

% calculate mean f1 score on S
resultsS = mean(results.f1S,4);
resultsS_rpca = squeeze(resultsS(1,:,:));
resultsS_opca = squeeze(resultsS(2,:,:));
resultsS_hsl = squeeze(resultsS(3,:,:));

% generate figure for success on L
h = figure; hold on;
set(h,'Position',[200 400 1100 258]);
ax = cell(3,1);
ax{1} = subplot(1,3,1); 
imagesc(successL_rpca); colormap gray;
title('Robust PCA','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{2} = subplot(1,3,2);
imagesc(successL_opca); colormap gray;
title('Outlier Pursuit','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{3} = subplot(1,3,3);
imagesc(successL_hsl); colormap gray;
title('Hybrid Subspace Learning','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
for i = 1:length(ax)
    a = ax{i};
    set(a,'TickLength',[0 0]);
    set(a,'XTick',1:11,'XTickLabel',{0:10:100});
    set(a,'YDir','normal','YTick',1:10,'YTickLabel',{10:10:100});
    set(a,'FontName',axisFont);
    xlabel(a,'s','FontSize',axisSize);
    ylabel(a,'k','Rotation',0,'FontSize',axisSize);
end
axes('Position',[0.075 0.135 0.9 0.77],'Visible','off');
caxis([0 1]); colorbar;
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/phase_transition_success_lowrank.pdf');

% generate figure for success on S
h = figure; hold on;
set(h,'Position',[200 400 1100 258]);
ax = cell(3,1);
ax{1} = subplot(1,3,1); 
imagesc(successS_rpca); colormap gray;
title('Robust PCA','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{2} = subplot(1,3,2);
imagesc(successS_opca); colormap gray;
title('Outlier Pursuit','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{3} = subplot(1,3,3);
imagesc(successS_hsl); colormap gray;
title('Hybrid Subspace Learning','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
for i = 1:length(ax)
    a = ax{i};
    set(a,'TickLength',[0 0]);
    set(a,'XTick',1:11,'XTickLabel',{0:10:100});
    set(a,'YDir','normal','YTick',1:10,'YTickLabel',{10:10:100});
    set(a,'FontName',axisFont);
    xlabel(a,'s','FontSize',axisSize);
    ylabel(a,'k','Rotation',0,'FontSize',axisSize);
end
axes('Position',[0.075 0.135 0.9 0.77],'Visible','off');
caxis([0 1]); colorbar;
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/phase_transition_success_sparse.pdf');

% generate figure for success on both tasks
h = figure; hold on;
set(h,'Position',[200 400 1100 258]);
ax = cell(3,1);
ax{1} = subplot(1,3,1); 
imagesc(successBoth_rpca); colormap gray;
title('Robust PCA','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{2} = subplot(1,3,2);
imagesc(successBoth_opca); colormap gray;
title('Outlier Pursuit','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{3} = subplot(1,3,3);
imagesc(successBoth_hsl); colormap gray;
title('Hybrid Subspace Learning','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
for i = 1:length(ax)
    a = ax{i};
    set(a,'TickLength',[0 0]);
    set(a,'XTick',1:11,'XTickLabel',{0:10:100});
    set(a,'YDir','normal','YTick',1:10,'YTickLabel',{10:10:100});
    set(a,'FontName',axisFont);
    xlabel(a,'s','FontSize',axisSize);
    ylabel(a,'k','Rotation',0,'FontSize',axisSize);
end
axes('Position',[0.075 0.135 0.9 0.77],'Visible','off');
caxis([0 1]); colorbar;
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/phase_transition_success_both.pdf');

% generate figure for results on L
h = figure; hold on;
set(h,'Position',[200 400 1100 258]);
ax = cell(3,1);
ax{1} = subplot(1,3,1); 
imagesc(resultsL_rpca); caxis([0 10]);
title('Robust PCA','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{2} = subplot(1,3,2);
imagesc(resultsL_opca); caxis([0 10]);
title('Outlier Pursuit','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{3} = subplot(1,3,3);
imagesc(resultsL_hsl); caxis([0 10]);
title('Hybrid Subspace Learning','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
colormap gray;
colormap(flipud(colormap)); 
for i = 1:length(ax)
    a = ax{i};
    set(a,'TickLength',[0 0]);
    set(a,'XTick',1:11,'XTickLabel',{0:10:100});
    set(a,'YDir','normal','YTick',1:10,'YTickLabel',{10:10:100});
    set(a,'FontName',axisFont);
    xlabel(a,'s','FontSize',axisSize);
    ylabel(a,'k','Rotation',0,'FontSize',axisSize);
end
axes('Position',[0.075 0.135 0.9 0.77],'Visible','off');
caxis([0 10]); colorbar;
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/phase_transition_errors_lowrank.pdf');

% generate figure for results on S
h = figure; hold on;
set(h,'Position',[200 400 1100 258]);
ax = cell(3,1);
ax{1} = subplot(1,3,1); 
imagesc(1-resultsS_rpca); caxis([0 1]);
title('Robust PCA','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{2} = subplot(1,3,2);
imagesc(1-resultsS_opca); caxis([0 1]);
title('Outlier Pursuit','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
ax{3} = subplot(1,3,3);
imagesc(1-resultsS_hsl); caxis([0 1]);
title('Hybrid Subspace Learning','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
colormap gray;
colormap(flipud(colormap));
for i = 1:length(ax)
    a = ax{i};
    set(a,'TickLength',[0 0]);
    set(a,'XTick',1:11,'XTickLabel',{0:10:100});
    set(a,'YDir','normal','YTick',1:10,'YTickLabel',{10:10:100});
    set(a,'FontName',axisFont);
    xlabel(a,'s','FontSize',axisSize);
    ylabel(a,'k','Rotation',0,'FontSize',axisSize);
end
axes('Position',[0.075 0.135 0.9 0.77],'Visible','off');
caxis([0 1]); colorbar;
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/phase_transition_errors_sparse.pdf');
