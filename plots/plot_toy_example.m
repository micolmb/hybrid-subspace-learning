% Script to plot base for Figure 1

h = figure;
X1 = [.2 .8 ; .4 1];
X2 = [.4 .2 ; 1 .8];
X3 = [0 0 ; .8 .8];
surf(X1,X2,X3,ones(size(X3)),'FaceAlpha',0.5)
xlim([0 1]); ylim([0 1]); zlim([0 1]);
set(gca,'XTick',0:0.2:1,'YTick',0:0.2:1,'ZTick',0:0.2:1);
set(gca,'XTickLabel',{},'YTickLabel',{},'ZTickLabel',{});
set(gca,'TickLength',[0 0]);
text(.5,-.025,-.075,'$x_1$','interpreter','latex','FontSize',20);
text(-.05,.6,-.1,'$x_2$','interpreter','latex','FontSize',20);
text(-.065,1.1,.45,'$x_3$','interpreter','latex','FontSize',20);
p = get(h,'Position');
p(3) = round(0.9*p(3));
set(h,'Position',p);

h = figure;
X1 = [.2 .8 ; .2 .8];
X2 = [.6 .4 ; .6 .4];
X3 = [0 0 ; 1 1];
surf(X1,X2,X3,ones(size(X3)),'FaceAlpha',0.5)
xlim([0 1]); ylim([0 1]); zlim([0 1]);
set(gca,'XTick',0:0.2:1,'YTick',0:0.2:1,'ZTick',0:0.2:1);
set(gca,'XTickLabel',{},'YTickLabel',{},'ZTickLabel',{});
set(gca,'TickLength',[0 0]);
text(.5,-.025,-.075,'$x_1$','interpreter','latex','FontSize',20);
text(-.05,.6,-.1,'$x_2$','interpreter','latex','FontSize',20);
text(-.065,1.1,.45,'$x_3$','interpreter','latex','FontSize',20);
p = get(h,'Position');
p(3) = round(0.9*p(3));
set(h,'Position',p);

% % set n
% n = 50;
% 
% % generate n random points in 2D
% Z = rand(n,2); % maybe they shouldn't be quite random, but have some shape?
% 
% % generate projection matrix
% A = rand(2,3);
% M = rand(1,3);
% 
% % calculate projection of points in 3D
% X = Z*A + ones(n,1)*M;
% 
% % calculate the vertices of the plane
% gridSize = 1;
% [Z1,Z2] = meshgrid(0:gridSize:1,0:gridSize:1);
% X1 = zeros(size(Z1));
% X2 = zeros(size(Z1));
% X3 = zeros(size(Z1));
% for i = 1:size(Z1,1)
%     for j = 1:size(Z1,2)
%         currz1 = Z1(i,j);
%         currz2 = Z2(i,j);
%         currx = [currz1 currz2]*A + M;
%         X1(i,j) = currx(1);
%         X2(i,j) = currx(2);
%         X3(i,j) = currx(3);
%     end
% end
% 
% % generate the plot
% figure;
% surf(X1,X2,X3,ones(size(X3)),'FaceAlpha',0.5);
% hold on;
% scatter3(X(:,1),X(:,2),X(:,3),'filled');


