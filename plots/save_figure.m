function save_figure(h,fileName)

    set(h,'Units','inches');
    screenposition = get(h,'Position');
    set(h,'PaperPosition',[0 0 screenposition(3:4)],'PaperSize',[screenposition(3:4)]);
    print('-dpdf','-painters',fileName);

end