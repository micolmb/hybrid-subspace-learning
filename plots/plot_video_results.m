% Script to plot results on video background subtraction

% set results directory
resultsDir = '~/Dropbox/hybrid_subspace/video_results';

% set frame size
frameSize = [130 160];

% load HSL results
hslLambda = 105.5;
rh = load(fullfile(resultsDir,sprintf('results_hsl_lambda%g.mat',hslLambda)));
rh.L = rh.Z*rh.A;
rh.S = rh.W*diag(rh.b);
rh.sparseMap = reshape(rh.b~=0,frameSize);

% load Outlier PCA results
opcaLambda = 0.15;
ro = load(fullfile(resultsDir,sprintf('results_opca_lambda%g.mat',opcaLambda)));
ro.sparseMap = reshape(sum(abs(ro.S))~=0,frameSize);

% load Robust PCA results
rpcaLambda = 0.012;
rr = load(fullfile(resultsDir,sprintf('results_rpca_lambda%g.mat',rpcaLambda)));

% generate figure for one frame
h = figure(1); hold on;
set(h,'Position',[95 209 825 489]);
i = 38;
currL = rh.L(i,:)*rh.div + rh.mu;
currS = rh.S(i,:)*rh.div + rh.mu;
currL = reshape(currL,frameSize);
currS = reshape(currS,frameSize);
subplot(3,4,1); imshow(uint8(reshape(rh.mu,frameSize)));
subplot(3,4,2); imshow(uint8(currL));
subplot(3,4,3); imshow(uint8(currS));
subplot(3,4,4); imshow(uint8(255*rh.sparseMap));
currL = ro.L(i,:)*ro.div + ro.mu;
currS = ro.S(i,:)*ro.div + ro.mu;
currL = reshape(currL,frameSize);
currS = reshape(currS,frameSize);
subplot(3,4,5); imshow(uint8(reshape(ro.mu,frameSize)));
subplot(3,4,6); imshow(uint8(currL));
subplot(3,4,7); imshow(uint8(currS));
subplot(3,4,8); imshow(uint8(255*ro.sparseMap));
currL = rr.L(i,:)*rr.div + rr.mu;
currS = rr.S(i,:)*rr.div + rr.mu;
currL = reshape(currL,frameSize);
currS = reshape(currS,frameSize);
sparseMap = reshape(rr.S(i,:)~=0,frameSize);
subplot(3,4,9); imshow(uint8(reshape(rr.mu,frameSize)));
subplot(3,4,10); imshow(uint8(currL));
subplot(3,4,11); imshow(uint8(currS));
subplot(3,4,12); imshow(uint8(255*sparseMap));
ax = axes('position',[0,0,1,1],'visible','off');
tx = text(0.1,0.83,{'Hybrid','Subspace','Learning'});
set(tx,'FontSize',13,'FontName','AvantGarde','FontWeight','bold','HorizontalAlignment','right')
tx = text(0.1,0.53,{'Outlier','Pursuit'});
set(tx,'FontSize',13,'FontName','AvantGarde','FontWeight','bold','HorizontalAlignment','right')
tx = text(0.1,0.22,{'Robust','PCA'});
set(tx,'FontSize',13,'FontName','AvantGarde','FontWeight','bold','HorizontalAlignment','right')
tx = text(0.21,0.96,'Mean');
set(tx,'FontSize',13,'FontName','AvantGarde','FontWeight','bold','HorizontalAlignment','center')
tx = text(0.42,0.96,'Low-Rank');
set(tx,'FontSize',13,'FontName','AvantGarde','FontWeight','bold','HorizontalAlignment','center')
tx = text(0.615,0.96,'High-Dim / Sparse');
set(tx,'FontSize',13,'FontName','AvantGarde','FontWeight','bold','HorizontalAlignment','center')
tx = text(0.82,0.96,'Sparsity Map');
set(tx,'FontSize',13,'FontName','AvantGarde','FontWeight','bold','HorizontalAlignment','center')
save_figure(h,'~/Dropbox/hybrid_subspace/video_results/escalator_frame.pdf');

% generate video
videoDir = '~/Dropbox/hybrid_subspace/video_results/video_frames/';
whiteCol = 255*ones(frameSize(1),20);
whiteRow = 255*ones(20,frameSize(2)*4 + 100);
for i = 1:size(rh.X,1)
    
    currL = rh.L(i,:)*rh.div + rh.mu;
    currS = rh.S(i,:)*rh.div + rh.mu;
    currL = reshape(currL,frameSize);
    currS = reshape(currS,frameSize);
    imgHSL = uint8([whiteCol reshape(rh.mu,frameSize) whiteCol currL whiteCol currS whiteCol 255*rh.sparseMap whiteCol]);

    currL = ro.L(i,:)*ro.div + ro.mu;
    currS = ro.S(i,:)*ro.div + ro.mu;
    currL = reshape(currL,frameSize);
    currS = reshape(currS,frameSize);
    imgOPCA = uint8([whiteCol reshape(ro.mu,frameSize) whiteCol currL whiteCol currS whiteCol 255*ro.sparseMap whiteCol]);
    
    currL = rr.L(i,:)*rr.div + rr.mu;
    currS = rr.S(i,:)*rr.div + rr.mu;
    currL = reshape(currL,frameSize);
    currS = reshape(currS,frameSize);
    sparseMap = reshape(rr.S(i,:)~=0,frameSize);
    imgRPCA = uint8([whiteCol reshape(rr.mu,frameSize) whiteCol currL whiteCol currS whiteCol 255*sparseMap whiteCol]);
    
    img = [whiteRow ; imgHSL ; whiteRow ; imgOPCA ; whiteRow ; imgRPCA ; whiteRow];
    img = [255*ones(size(img,1),80) img];
    img = [255*ones(30,size(img,2)) ; img];
    
    imwrite(img,fullfile(videoDir,sprintf('frame%d.jpg',i)));

end
imageNames = dir(fullfile(videoDir,'*.jpg'));
imageNames = {imageNames.name}';
outputVideo = VideoWriter(fullfile(videoDir,'escalator_video_annotated.avi'));
outputVideo.FrameRate = 10;
open(outputVideo)
for i = 1:length(imageNames)
   img = imread(fullfile(videoDir,sprintf('frame%d.jpg',i)));
   writeVideo(outputVideo,img)
end
close(outputVideo)

% show HSL results
h = figure(1); hold on;
set(h,'Position',[100 450 1025 225]);
for i = 1:size(rh.X,1)
    currL = rh.L(i,:)*rh.div + rh.mu;
    currS = rh.S(i,:)*rh.div + rh.mu;
    currL = reshape(currL,frameSize);
    currS = reshape(currS,frameSize);
    subplot(1,4,1); imshow(uint8(reshape(rh.mu,frameSize)));
    subplot(1,4,2); imshow(uint8(currL));
    subplot(1,4,3); imshow(uint8(currS));
    subplot(1,4,4); imshow(uint8(255*rh.sparseMap));
    pause(0.1);
end

% show OPCA results
h = figure(2); hold on;
set(h,'Position',[100 450 825 225]);
for i = 1:size(ro.X,1)
    currL = ro.L(i,:)*ro.div + ro.mu;
    currS = ro.S(i,:)*ro.div + ro.mu;
    currL = reshape(currL,frameSize);
    currS = reshape(currS,frameSize);
    subplot(1,4,1); imshow(uint8(reshape(ro.mu,frameSize)));
    subplot(1,4,2); imshow(uint8(currL));
    subplot(1,4,3); imshow(uint8(currS));
    subplot(1,4,4); imshow(uint8(255*ro.sparseMap));
    pause(0.1);
end

% show RPCA results
h = figure(3); hold on;
set(h,'Position',[100 450 825 225]);
for i = 1:size(rr.X,1)
    currL = rr.L(i,:)*rr.div + rr.mu;
    currS = rr.S(i,:)*rr.div + rr.mu;
    currL = reshape(currL,frameSize);
    currS = reshape(currS,frameSize);
    sparseMap = reshape(rr.S(i,:)~=0,frameSize);
    subplot(1,4,1); imshow(uint8(reshape(rr.mu,frameSize)));
    subplot(1,4,2); imshow(uint8(currL));
    subplot(1,4,3); imshow(uint8(currS));
    subplot(1,4,4); imshow(uint8(255*sparseMap));
    pause(0.1);
end