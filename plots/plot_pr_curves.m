% Script to plot precision recall curves

% set results directory
resultsDir = '~/Dropbox/hybrid_subspace/synthetic_results';

% set plotting constants
axisFont = 'AvantGarde';
axisSize = 18;
axisWeight = 'normal';
titleFont = 'AvantGarde';
titleSize = 18;
titleWeight = 'bold';
legSize = 16;
lineWidth = 2;
tickSize = 14;
labelSize = 16;

% set line colors
lineColors = [.4 0 .9 ; 0 .5 .1 ; 1 .5 0 ; 1 0 0];

% set method order
ord = [3 1 2 4];

% load results for first pr curve
load(fullfile(resultsDir,'results_prcurve_1.mat'));

% plot first pr curve
h = figure; hold on;
bars = cell(4,1);
set(gca,'FontName',axisFont);
for b = 1:4
    ind = ord(b);
    bars{b} = errorbar(results.prcurveS{ind}.rec,results.prcurveS{ind}.prec,results.prcurveS{1}.stderr,'o-');
    set(bars{b}(1),'Color',lineColors(b,:),'LineWidth',2);
    set(bars{b},'MarkerSize',5,'MarkerFaceColor',[.7 .7 .7]);
end
xlim([-0.02 1.02]); ylim([-0.02 1.02]);
xlabel('Recall','FontSize',axisSize,'FontWeight',axisWeight);
ylabel('Precision','FontSize',axisSize,'FontWeight',axisWeight);
title('Average Precision Recall Curves','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
l = legend({sprintf('Sparse PCA (AUCPR = %.2f +/- %.2f)',mean(results.aucprS(ord(1),:),2),std(results.aucprS(ord(1),:),[],2)),...
    sprintf('Robust PCA (AUCPR = %.2f +/- %.2f)',mean(results.aucprS(ord(2),:),2),std(results.aucprS(ord(2),:),[],2)),...
    sprintf('Outlier Pursuit (AUCPR = %.2f +/- %.2f)',mean(results.aucprS(ord(3),:),2),std(results.aucprS(ord(3),:),[],2)),...
    sprintf('Hybrid SL (AUCPR = %.2f +/- %.2f)',mean(results.aucprS(ord(4),:),2),std(results.aucprS(ord(4),:),[],2))});
set(l,'FontSize',legSize,'Location','SouthWest');
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/pr_curve_easy.pdf');

% load results for second pr curve
load(fullfile(resultsDir,'results_prcurve_2.mat'));

% plot second pr curve
h = figure; hold on;
bars = cell(4,1);
set(gca,'FontName',axisFont);
for b = 1:4
    ind = ord(b);
    bars{b} = errorbar(results.prcurveS{ind}.rec,results.prcurveS{ind}.prec,results.prcurveS{1}.stderr,'o-');
    set(bars{b}(1),'Color',lineColors(b,:),'LineWidth',2);
    set(bars{b},'MarkerSize',5,'MarkerFaceColor',[.7 .7 .7]);
end
xlim([-0.02 1.02]); ylim([-0.02 1.02]);
xlabel('Recall','FontSize',axisSize,'FontWeight',axisWeight);
ylabel('Precision','FontSize',axisSize,'FontWeight',axisWeight);
title('Average Precision Recall Curves','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
l = legend({sprintf('Sparse PCA (AUCPR = %.2f +/- %.2f)',mean(results.aucprS(ord(1),:),2),std(results.aucprS(ord(1),:),[],2)),...
    sprintf('Robust PCA (AUCPR = %.2f +/- %.2f)',mean(results.aucprS(ord(2),:),2),std(results.aucprS(ord(2),:),[],2)),...
    sprintf('Outlier Pursuit (AUCPR = %.2f +/- %.2f)',mean(results.aucprS(ord(3),:),2),std(results.aucprS(ord(3),:),[],2)),...
    sprintf('Hybrid SL (AUCPR = %.2f +/- %.2f)',mean(results.aucprS(ord(4),:),2),std(results.aucprS(ord(4),:),[],2))});
set(l,'FontSize',legSize,'Location','SouthWest');
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/pr_curve_hard.pdf');

% % load f1 score results
% load(fullfile(resultsDir,'results_vary_theta_split.mat'));
% 
% h = figure; hold on;
% b = errorbar(mean(results.f1S(ord+1,:,:),3)',std(results.f1S(ord+1,:,:),[],3)','o-','LineWidth',lineWidth);
% set(gca,'XTick',1:11,'XTickLabel',{'1','.9','.8','.7','.6','.5','.4','.3','.2','.1','0'},'FontSize',tickSize);
% xlim([0.5 11.5]); ylim([0 1]);
% xlabel('Participation in LowR (\theta_1)','FontSize',labelSize,'FontName','AvantGarde');
% ylabel('F1 Score','FontSize',labelSize,'FontName','AvantGarde');

