% Script to generate plots for analyzing algorithm behavior
%
% Plots:
% (1) General convergence (with lower tolerance)
% (2) Difference between objective of cold and warm starts as gamma goes
% to gamma_max (show for theta=[.75,.25,0], but mention that instability 
% gets worse with larger number of high-dimensional features)
% (3) Difference between overlap and f1 score of cold and warm starts as
% gamma goes to gamma_max (show for theta=[.75,.25,0], but mention that
% instability gets worse with larger number of high-dimensional features)
% (4) Stability of minimum using warm starts
% (5) Plot of norms of columns of A after first iteration (gamma = 0) and
% after final iteration (gamma = gamma_max)

% set plotting constants
axisFont = 'AvantGarde';
axisSize = 15;
axisWeight = 'normal';
titleFont = 'AvantGarde';
titleSize = 15;
titleWeight = 'bold';

% set seed
rng(1013);

% set variables
n = 100;
p = 200;
k = 20;
sigma = 1;
theta = [.75 .25 0];

% generate synthetic data
[X,Z_true,A_true,W_true,b_true] = generate_synth_data_hybrid(n,p,k,sigma,theta);

% set lambda
lambda = 4;

% generate convergence data
option = struct('innertol',1e-3,'outertol',1e-2,'verbose',0);
%[Z,A,W,b,k,lambda,results] = hybrid_sl_select_params(X,k,[],struct('verbose',1));
%[Z,A,W,b,objVals,gammaVals,aicVals] = hybrid_sl_wrapper_exclusive(X,k,lambda,option);
[Z,A,W,b,objVals,iterVals] = hybrid_sl_optimization_accel(X,k,0,0,lambda,[],[],[],[],option);

% plot convergence
objValsPlot = objVals(1:300)./numel(X);
h = figure; hold on;
set(h,'Position',[100 100 325 275]);
plot(objValsPlot,'-','Color',[.3 .3 .3],'LineWidth',2);
xlim([-length(objValsPlot)*.01 length(objValsPlot)*1.01]);
set(gca,'FontName',axisFont);
xlabel('Iteration Number','FontSize',axisSize,'FontWeight',axisWeight);
ylabel('Objective Value','FontSize',axisSize,'FontWeight',axisWeight);
title('Convergence of HSL','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/convergence_example.pdf');

% generate stability data
option = struct('innertol',1e-4,'outertol',1e-2);
objFinalWarm = zeros(100,1);
for i = 1:100
    [Z,A,W,b,objVals] = hybrid_sl_wrapper_exclusive(X,k,lambda,option);
    objFinalWarm(i) = objVals(end);
end
objFinalCold = zeros(100,1);
for i = 1:100
    gamma = 0;
    while (true)
        [Z,A,W,b,objVals] = hybrid_sl_optimization_accel(X,k,gamma,0,lambda,[],[],[],[],option);
        if sum(sum(abs(A)).*abs(b')) == 0; break;
        elseif gamma == 0; gamma = 1;
        else gamma = gamma*2;
        end
    end
    objFinalCold(i) = objVals(end);
end

% plot stability
h = figure; hold on;
set(h,'Position',[100 100 325 275]);
b = boxplot([objFinalWarm,objFinalCold]./numel(X),'colors','rb','medianstyle','target','labels',{'Warm Starts','Cold Starts'});
for i = 1:numel(b)
    set(b(i),'linewidth',1.4);
end
%ylim([0 1]);
set(gca,'FontName',axisFont);
set(findobj(gca,'Type','text'),'FontName',axisFont,'FontSize',axisSize,'FontWeight',axisWeight,'VerticalAlignment','middle')
ylabel('Final Objective Value','FontSize',axisSize,'FontWeight',axisWeight);
title('Stability of Warm/Cold Starts','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/stability_example.pdf');

% generate warm vs cold start data
maxGamma = 200;
objWarm = zeros(maxGamma+1,1);
objCold = zeros(maxGamma+1,1);
f1Warm = zeros(maxGamma+1,3);
f1Cold = zeros(maxGamma+1,3);
overlapWarm = zeros(maxGamma+1,1);
overlapCold = zeros(maxGamma+1,1);
Z_warm = [];
A_warm = [];
W_warm = [];
b_warm = [];
for gamma = 0:200
    [Z_warm,A_warm,W_warm,b_warm,objVals] = hybrid_sl_optimization_accel(X,k,gamma,0,lambda,Z_warm,A_warm,W_warm,b_warm,option);
    normsA_warm = sqrt(sum(A_warm.^2));
    [pr,re,f1] = calc_assignment_err(b_true~=0,b_warm~=0);
    f1Warm(gamma+1,:) = [pr re f1];
    overlapWarm(gamma+1) = sum(normsA_warm.*b_warm' ~= 0)/length(b_warm);
    objWarm(gamma+1) = objVals(end);
    
    [Z_cold,A_cold,W_cold,b_cold,objVals] = hybrid_sl_optimization_accel(X,k,gamma,0,lambda,[],[],[],[],option);
    normsA_cold = sqrt(sum(A_cold.^2));
    [pr,re,f1] = calc_assignment_err(b_true~=0,b_cold~=0);
    f1Cold(gamma+1,:) = [pr re f1];
    overlapCold(gamma+1) = sum(normsA_cold.*b_cold' ~= 0)/length(b_cold);
    objCold(gamma+1) = objVals(end);
end

% plot objective of warm vs cold starts
h = figure; hold on;
set(h,'Position',[100 100 325 275]);
plot(objWarm./numel(X),'r-','LineWidth',1.4);
plot(objCold./numel(X),'b-','LineWidth',1.2);
box on;
xlim([0 200]);
set(gca,'FontName',axisFont);
xlabel('Value of \gamma','FontSize',axisSize,'FontWeight',axisWeight);
ylabel('Objective Value','FontSize',axisSize,'FontWeight',axisWeight);
title('Objective of Warm/Cold Starts','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
legend({'Warm Starts','Cold Starts'},'FontSize',axisSize,'FontWeight',axisWeight,'FontName',axisFont);
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/warm_cold_obj.pdf');

% plot f1 score of warm vs cold starts
h = figure; hold on;
set(h,'Position',[100 100 325 275]);
plot(f1Warm(:,3),'r-','LineWidth',1.4);
plot(f1Cold(:,3),'b-','LineWidth',1.2);
xlim([0 200]);
set(gca,'FontName',axisFont);
xlabel('Value of \gamma','FontSize',axisSize,'FontWeight',axisWeight);
ylabel('F1 Score','FontSize',axisSize,'FontWeight',axisWeight);
title('F1 Score of Warm/Cold Starts','FontSize',titleSize,'FontWeight',titleWeight,'FontName',titleFont);
legend({'Warm Starts','Cold Starts'},'FontSize',axisSize,'FontWeight',axisWeight,'FontName',axisFont);
save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/warm_cold_f1.pdf');


