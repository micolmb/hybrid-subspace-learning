% Script to plot synthetic data experiment results

% set plotting constants
axisFont = 'AvantGarde';
axisSize = 15;
axisWeight = 'normal';
titleFont = 'AvantGarde';
titleSize = 18;
titleWeight = 'bold';
legSize = 13;
lineWidth = 2;
tickSize = 14;
labelSize = 16;

% set line colors
lineColors = [0 0 1 ; .4 0 .9 ; 0 .5 .1 ; 1 .5 0 ; 1 0 0];

% set method order
ord = [1 4 2 3 5];

% load results
resultsDir = '~/Dropbox/hybrid_subspace/synthetic_results';
varyNoise = load(fullfile(resultsDir,'results_vary_noise.mat'));
varyK = load(fullfile(resultsDir,'results_vary_k.mat'));
varySplit = load(fullfile(resultsDir,'results_vary_theta_split.mat'));
varyOverlap = load(fullfile(resultsDir,'results_vary_theta_overlap.mat'));

h = figure; hold on;
set(h,'Position',[10 10 1300 650]);
axes = cell(12,1);
bars = cell(12,1);

axes{1} = subplot('position',[.04 .71 .205 .22]); hold on; box on;
bars{1} = errorbar(mean(varyNoise.results.errLsub(ord,:,:),3)',std(varyNoise.results.errLsub(ord,:,:),[],3)','o-','LineWidth',lineWidth);
l = legend({'PCA','Sparse PCA','Robust PCA','Outlier Pursuit','Hybrid SL'});
set(l,'FontSize',legSize,'Location','NorthWest');
set(gca,'XTick',1:9,'XTickLabel',{'0','0.1','0.2','0.5','1','2','5','10','20'},'FontSize',tickSize);
xlim([0.5 9.5]); ylim([0 7]);
xlabel('Variance of Error (\sigma^2)','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of L','FontSize',labelSize,'FontName','AvantGarde');
t = title('Varying Noise','FontSize',titleSize,'FontWeight',titleWeight,'FontName','AvantGarde');

axes{2} = subplot('position',[.285 .71 .205 .22]); hold on; box on;
bars{2} = errorbar(mean(varyK.results.errLsub(ord,:,:),3)',std(varyK.results.errLsub(ord,:,:),[],3)','o-','LineWidth',lineWidth);
set(gca,'XTick',1:6,'XTickLabel',{'2','5','10','20','50','100'},'FontSize',tickSize);
xlim([0.5 6.5]); ylim([0 7]);
xlabel('Low-rank dimension k','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of L','FontSize',labelSize,'FontName','AvantGarde');
set(get(axes{2},'XLabel'),'Units','pixels')
posLabel = get(get(axes{2},'XLabel'),'Position');
set(get(axes{2},'XLabel'),'Position',posLabel+[0 -3 0])
t = title('Varying Dimensionality','FontSize',titleSize,'FontWeight',titleWeight,'FontName','AvantGarde');

axes{3} = subplot('position',[.53 .71 .205 .22]); hold on; box on;
bars{3} = errorbar(mean(varySplit.results.errLsub(ord,:,:),3)',std(varySplit.results.errLsub(ord,:,:),[],3)','o-','LineWidth',lineWidth);
set(gca,'XTick',1:11,'XTickLabel',{'1','.9','.8','.7','.6','.5','.4','.3','.2','.1','0'},'FontSize',tickSize);
xlim([0.5 11.5]); ylim([0 7]);
xlabel('Participation in LowR (\theta_1)','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of L','FontSize',labelSize,'FontName','AvantGarde');
set(get(axes{3},'XLabel'),'Units','pixels')
posLabel = get(get(axes{3},'XLabel'),'Position');
set(get(axes{3},'XLabel'),'Position',posLabel+[0 -7 0])
t = title('Varying Split of LowR v. HighD','FontSize',titleSize,'FontWeight',titleWeight,'FontName','AvantGarde');

axes{4} = subplot('position',[.775 .71 .205 .22]); hold on; box on;
bars{4} = errorbar(mean(varyOverlap.results.errLsub(ord,:,:),3)',std(varyOverlap.results.errLsub(ord,:,:),[],3)','o-','LineWidth',lineWidth);
set(gca,'XTick',1:11,'XTickLabel',{'0','.1','.2','.3','.4','.5','.6','.7','.8','.9','1'},'FontSize',tickSize);
xlim([0.5 11.5]); ylim([0 7]);
xlabel('Amount of Overlap (\theta_3)','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of L','FontSize',labelSize,'FontName','AvantGarde');
set(get(axes{4},'XLabel'),'Units','pixels')
posLabel = get(get(axes{4},'XLabel'),'Position');
set(get(axes{4},'XLabel'),'Position',posLabel+[0 -7 0])
t = title('Varying Overlap of LowR & HighD','FontSize',titleSize,'FontWeight',titleWeight,'FontName','AvantGarde');


axes{5} = subplot('position',[.04 .4 .205 .22]); hold on; box on;
bars{5} = errorbar(mean(varyNoise.results.errSfull(ord,:,:),3)',std(varyNoise.results.errSfull(ord,:,:),[],3)','o-','LineWidth',lineWidth);
%l = legend({'PCA','Robust PCA','Outlier Pursuit','Sparse PCA','Hybrid SL'});
%set(l,'FontSize',legFS,'Location','NorthWest');
set(gca,'XTick',1:9,'XTickLabel',{'0','0.1','0.2','0.5','1','2','5','10','20'},'FontSize',tickSize);
xlim([0.5 9.5]); ylim([0 5]);
xlabel('Variance of Error (\sigma^2)','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of S','FontSize',labelSize,'FontName','AvantGarde');

axes{6} = subplot('position',[.285 .4 .205 .22]); hold on; box on;
bars{6} = errorbar(mean(varyK.results.errSfull(ord,:,:),3)',std(varyK.results.errSfull(ord,:,:),[],3)','o-','LineWidth',lineWidth);
set(gca,'XTick',1:6,'XTickLabel',{'2','5','10','20','50','100'},'FontSize',tickSize);
xlim([0.5 6.5]); ylim([0 5]);
xlabel('Low-rank dimension k','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of S','FontSize',labelSize,'FontName','AvantGarde');
set(get(axes{6},'XLabel'),'Units','pixels')
posLabel = get(get(axes{6},'XLabel'),'Position');
set(get(axes{6},'XLabel'),'Position',posLabel+[0 -3 0])

axes{7} = subplot('position',[.53 .4 .205 .22]); hold on; box on;
bars{7} = errorbar(mean(varySplit.results.errSfull(ord,:,:),3)',std(varySplit.results.errSfull(ord,:,:),[],3)','o-','LineWidth',lineWidth);
set(gca,'XTick',1:11,'XTickLabel',{'1','.9','.8','.7','.6','.5','.4','.3','.2','.1','0'},'FontSize',tickSize);
xlim([0.5 11.5]); ylim([0 5]);
xlabel('Participation in LowR (\theta_1)','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of S','FontSize',labelSize,'FontName','AvantGarde');
set(get(axes{7},'XLabel'),'Units','pixels')
posLabel = get(get(axes{7},'XLabel'),'Position');
set(get(axes{7},'XLabel'),'Position',posLabel+[0 -7 0])

axes{8} = subplot('position',[.775 .4 .205 .22]); hold on; box on;
bars{8} = errorbar(mean(varyOverlap.results.errSfull(ord,:,:),3)',std(varyOverlap.results.errSfull(ord,:,:),[],3)','o-','LineWidth',lineWidth);
set(gca,'XTick',1:11,'XTickLabel',{'0','.1','.2','.3','.4','.5','.6','.7','.8','.9','1'},'FontSize',tickSize);
xlim([0.5 11.5]); ylim([0 5]);
xlabel('Amount of Overlap (\theta_3)','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of S','FontSize',labelSize,'FontName','AvantGarde');
set(get(axes{8},'XLabel'),'Units','pixels')
posLabel = get(get(axes{8},'XLabel'),'Position');
set(get(axes{8},'XLabel'),'Position',posLabel+[0 -7 0])


axes{9} = subplot('position',[.04 .09 .205 .22]); hold on; box on;
bars{9} = errorbar(mean(varyNoise.results.errXobs(ord,:,:),3)',std(varyNoise.results.errXobs(ord,:,:),[],3)','o-','LineWidth',lineWidth);
%l = legend({'PCA','Robust PCA','Outlier Pursuit','Sparse PCA','Hybrid SL'});
%set(l,'FontSize',legFS,'Location','NorthWest');
set(gca,'XTick',1:9,'XTickLabel',{'0','0.1','0.2','0.5','1','2','5','10','20'},'FontSize',tickSize);
xlim([0.5 9.5]); ylim([0 5]);
xlabel('Variance of Error (\sigma^2)','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of X','FontSize',labelSize,'FontName','AvantGarde');

axes{10} = subplot('position',[.285 .09 .205 .22]); hold on; box on;
bars{10} = errorbar(mean(varyK.results.errXobs(ord,:,:),3)',std(varyK.results.errXobs(ord,:,:),[],3)','o-','LineWidth',lineWidth);
set(gca,'XTick',1:6,'XTickLabel',{'2','5','10','20','50','100'},'FontSize',tickSize);
xlim([0.5 6.5]); ylim([0 5]);
xlabel('Low-rank dimension k','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of X','FontSize',labelSize,'FontName','AvantGarde');
set(get(axes{10},'XLabel'),'Units','pixels')
posLabel = get(get(axes{10},'XLabel'),'Position');
set(get(axes{10},'XLabel'),'Position',posLabel+[0 -3 0])

axes{11} = subplot('position',[.53 .09 .205 .22]); hold on; box on;
bars{11} = errorbar(mean(varySplit.results.errXobs(ord,:,:),3)',std(varySplit.results.errXobs(ord,:,:),[],3)','o-','LineWidth',lineWidth);
set(gca,'XTick',1:11,'XTickLabel',{'1','.9','.8','.7','.6','.5','.4','.3','.2','.1','0'},'FontSize',tickSize);
xlim([0.5 11.5]); ylim([0 5]);
xlabel('Participation in LowR (\theta_1)','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of X','FontSize',labelSize,'FontName','AvantGarde');
set(get(axes{11},'XLabel'),'Units','pixels')
posLabel = get(get(axes{11},'XLabel'),'Position');
set(get(axes{11},'XLabel'),'Position',posLabel+[0 -7 0])

axes{12} = subplot('position',[.775 .09 .205 .22]); hold on; box on;
bars{12} = errorbar(mean(varyOverlap.results.errXobs(ord,:,:),3)',std(varyOverlap.results.errXobs(ord,:,:),[],3)','o-','LineWidth',lineWidth);
set(gca,'XTick',1:11,'XTickLabel',{'0','.1','.2','.3','.4','.5','.6','.7','.8','.9','1'},'FontSize',tickSize);
xlim([0.5 11.5]); ylim([0 5]);
xlabel('Amount of Overlap (\theta_3)','FontSize',labelSize,'FontName','AvantGarde');
ylabel('Recovery Error of X','FontSize',labelSize,'FontName','AvantGarde');
set(get(axes{12},'XLabel'),'Units','pixels')
posLabel = get(get(axes{12},'XLabel'),'Position');
set(get(axes{12},'XLabel'),'Position',posLabel+[0 -7 0])

for a = 1:12
    %set(axes{a},'YGrid','on');
    set(axes{a},'box','off','FontName','AvantGarde');    
    set(bars{a},'MarkerSize',5,'MarkerFaceColor',[.7 .7 .7]);
end

for b = 1:12
    for i = 1:5
        set(bars{b}(i),'Color',lineColors(i,:));
    end
end

save_figure(h,'~/Dropbox/hybrid_subspace/synthetic_results/synth_experiments.pdf');