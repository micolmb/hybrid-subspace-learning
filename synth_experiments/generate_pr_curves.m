% Function to generate precision/recall curves for recovery of the sparse
% features using hybrid subspace learning and several baselines, including:
%   (1) Robust PCA (Candes et al., 2011)
%   (2) Outlier Pursuit (Xu et al., 2010)
%   (3) Sparse PCA (Zou et al., 2006)
%
% Inputs:
%   seed: random generator seed
%   n: number of samples
%   p: total number of features
%   k: number of features for low-rank component
%   sigma: variance of the noise
%   theta: 3-dimensional vector of parameters specifying 
%       (1) the probability that features participate in low-rank component, 
%       (2) the probability that features participate in sparse component, 
%       (3) the probability that features participate in both components 
%   dataType: the type of synthetic data to be generated
%       'hybrid' = data generated directly from hybrid model
%       'outlier' = data generated with hybrid structure and outliers
%       'sparse' = data generated with hybrid structure and extra sparsity in A
%   methodType: the type of hybrid subspace learning method to be used
%       1 = push gamma to gamma_max, enforce no overlap between components
%       2 = select best gamma, allow overlap between components
%   outputDir: directory where results should be saved
%   outputFile: file where results should be saved
%
% Outputs:
%   results: struct containing results of all methods

function results = generate_pr_curves(seed,n,p,k,sigma,theta,dataType,methodType,outputDir,outputFile)

    % add paths
    addpath(genpath('.'));
    
    % set seed
    rng(seed);

    % generate data
    if strcmp(dataType,'hybrid')
        [X_obs,Z_true,A_true,W_true,b_true] = generate_synth_data_hybrid(n,p,k,sigma,theta);
        L_true = Z_true*A_true;
        S_true = W_true*diag(b_true);
        X_true = L_true + S_true;
    elseif strcmp(dataType,'outlier')
        [X_obs,Z_true,A_true,W_true,b_true,O_true] = generate_synth_data_outlier(n,p,k,sigma,theta);
        L_true = Z_true*A_true;
        S_true = W_true*diag(b_true) + O_true;
        X_true = L_true + S_true;
    elseif strcmp(dataType,'sparse')
        [X_obs,Z_true,A_true,W_true,b_true] = generate_synth_data_sparse(n,p,k,sigma,theta);
        L_true = Z_true*A_true;
        S_true = W_true*diag(b_true);
        X_true = L_true + S_true;
    else
        error('Error: The specified data type was not recognized');
    end

    % check method
    if methodType == 2
        display('Selecting best gamma is not currently supported. Using methodType = 1.')
    elseif methodType ~= 1
        error('Error: The specified method type was not recognized')
    end
    
    % calculate true high-dimensional indices
    S_ind_true = (b_true ~= 0)';
    
    % initialize results
    results = [];
    
    % run Robust PCA, calculate PR curve
    results.rpca = get_robust_pca_prcurve(X_obs,k,S_ind_true,L_true,S_true);

    % run Outlier PCA, calculate PR curve
    results.opca = get_outlier_pca_prcurve(X_obs,k,S_ind_true,L_true,S_true);

    % run Sparse PCA, calculate PR curve
    results.spca = get_sparse_pca_prcurve(X_obs,k,S_ind_true,L_true);

    % run Hybrid SL, calculate PR curve
    results.hsl = get_hybrid_sl_prcurve(X_obs,k,S_ind_true,L_true,S_true);
    
    %figure; hold on;
    %plot(results.rpca.prcurve(:,2),results.rpca.prcurve(:,1),'ro-');
    %plot(results.opca.prcurve(:,2),results.opca.prcurve(:,1),'yo-');
    %plot(results.spca.prcurve(:,2),results.spca.prcurve(:,1),'bo-');
    %plot(results.hsl.prcurve(:,2),results.hsl.prcurve(:,1),'go-');
    %fprintf('AUCPR: rpca = %g, opca = %g, spca = %g, hsl = %g\n',results.rpca.aucpr,results.opca.aucpr,results.spca.aucpr,results.hsl.aucpr);
    
    % save results
    save(fullfile(outputDir,outputFile));

end

% Function to run Robust PCA on X and calculate the PR curve
function [results] = get_robust_pca_prcurve(X,k,S_ind_true,L_true,S_true)

    % run Robust PCA, use default lambda = 1/sqrt(max(n,p))
    [L_rpca,S_rpca] = robustpca(X);
    norms = sqrt(sum(S_rpca.^2));
    
    % calculate errors
    err_L_sub = calc_subspace_err(L_true,L_rpca,k);
    err_S_full = calc_recovery_err(S_true,S_rpca);
    err_X_obs = calc_recovery_err(X,L_rpca + S_rpca);

    % set threshold values
    step = max(norms)/50;
    threshVals = 0:step:max(norms)+step;

    % initialize pr curve
    prcurve = zeros(length(threshVals),3);

    % calculate PR curve
    for i = 1:length(threshVals)
        thresh = threshVals(i);
        S_ind_rpca = norms >= thresh;
        [prec,rec,f1] = calc_assignment_err(S_ind_true,S_ind_rpca);
        prcurve(i,:) = [prec rec f1];
    end

    % calculate AUCPR
    aucpr = calc_aucpr(prcurve(:,1),prcurve(:,2));
    
    % combine results
    results = struct('prcurve',prcurve,'aucpr',aucpr,...
        'errLsub',err_L_sub,'errSfull',err_S_full,'errXobs',err_X_obs);
    
%     figure; plot(prcurve(:,2),prcurve(:,1),'o-');
%     xlabel('Recall','FontSize',13); ylabel('Precision','FontSize',13);
%     xlim([0 1]); ylim([0 1]);
%     title('Robust PCA','FontSize',14);
        
end

% Function to run Outlier PCA on X and calculate the PR curve
function [results] = get_outlier_pca_prcurve(X,k,S_ind_true,L_true,S_true)

    % set lambda values
    numOutlier = sum(S_ind_true);
    defaultLambda = 3/(7*sqrt(numOutlier));
    lambdaVals = defaultLambda*(2.^(2:0.05:5));
    
    % initialize pr curve
    prcurve = zeros(length(lambdaVals),3);
    
    % initialize errors
    err_L_sub = zeros(length(lambdaVals),1);
    err_S_full = zeros(length(lambdaVals),1);
    err_X_obs = zeros(length(lambdaVals),1);
    
    % run Robust PCA with each value of lambda
    for i = 1:length(lambdaVals)
        lambda = lambdaVals(i);
        [L_opca,S_opca] = mrpca(X,ones(size(X)),lambda);
        S_ind_opca = sqrt(sum(S_opca.^2)) > 0;
        [prec,rec,f1] = calc_assignment_err(S_ind_true,S_ind_opca);
        prcurve(i,:) = [prec rec f1];
        err_L_sub(i) = calc_subspace_err(L_true,L_opca,k);
        err_S_full(i) = calc_recovery_err(S_true,S_opca);
        err_X_obs(i) = calc_recovery_err(X,L_opca + S_opca);
    end

    % calculate AUCPR
    aucpr = calc_aucpr(prcurve(:,1),prcurve(:,2));
    
    % combine results
    results = struct('prcurve',prcurve,'aucpr',aucpr,...
        'errLsub',err_L_sub,'errSfull',err_S_full,'errXobs',err_X_obs);
    
%     figure; plot(prcurve(:,2),prcurve(:,1),'o-');
%     xlabel('Recall','FontSize',13); ylabel('Precision','FontSize',13);
%     xlim([0 1]); ylim([0 1]);
%     title('Outlier Pursuit','FontSize',14);
      
end

% Function to run Sparse PCA on X and calculate the PR curve
function [results] = get_sparse_pca_prcurve(X,k,S_ind_true,L_true)

    % run Sparse PCA, specify the true number of nonzeros
    colNorms = sqrt(sum(X.^2));
    Xstd = X./repmat(colNorms,size(X,1),1);
    numFeature = sum(~S_ind_true);
    Ap_spca = spca(Xstd,[],k,Inf,-numFeature);
    A_spca = pinv(Ap_spca);
    Z_spca = Xstd*Ap_spca;
    L_spca = (Z_spca*A_spca).*repmat(colNorms,size(X,1),1);
    norms = sum(Ap_spca==0,2)';
    
    % calculate errors
    err_L_sub = calc_subspace_err(L_true,L_spca,k);
    err_X_obs = calc_recovery_err(X,L_spca);
    
    % set threshold values
    step = max(norms)/50;
    threshVals = 0:step:max(norms)+step;

    % initialize pr curve
    prcurve = zeros(length(threshVals),3);

    % calculate PR curve
    for i = 1:length(threshVals)
        thresh = threshVals(i);
        S_ind_spca = norms >= thresh;
        [prec,rec,f1] = calc_assignment_err(S_ind_true,S_ind_spca);
        prcurve(i,:) = [prec rec f1];
    end
    
    % calculate AUCPR
    aucpr = calc_aucpr(prcurve(:,1),prcurve(:,2));
    
    % combine results
    results = struct('prcurve',prcurve,'aucpr',aucpr,...
        'errLsub',err_L_sub,'errXobs',err_X_obs);
    
%     figure; plot(prcurve(:,2),prcurve(:,1),'o-');
%     xlabel('Recall','FontSize',13); ylabel('Precision','FontSize',13);
%     xlim([0 1]); ylim([0 1]);
%     title('Sparse PCA','FontSize',14);
    
end

% Function to run Outlier PCA on X and calculate the PR curve
function [results] = get_hybrid_sl_prcurve(X,k,S_ind_true,L_true,S_true)

    % set optimization options
    option = struct('innertol',1e-5,'outertol',1e-3);
    
    % set lambda values
    lambdaMax = max(sum(abs(X)));
    lambdaVals = [0 lambdaMax.*(2.^(-13:0))];
    
    % initialize pr curve
    prcurve = zeros(length(lambdaVals),3);
    
    % initialize errors
    err_L_sub = zeros(length(lambdaVals),1);
    err_S_full = zeros(length(lambdaVals),1);
    err_X_obs = zeros(length(lambdaVals),1);

    % run Hybrid SL with each value of lambda
    for i = 1:length(lambdaVals)
        lambda = lambdaVals(i);
        [Z_hsl,A_hsl,W_hsl,b_hsl] = hybrid_sl_wrapper_exclusive(X,k,lambda,option);
        S_ind_hsl = (abs(b_hsl) > 0)';
        [prec,rec,f1] = calc_assignment_err(S_ind_true,S_ind_hsl);
        prcurve(i,:) = [prec rec f1];
        err_L_sub(i) = calc_subspace_err(L_true,Z_hsl*A_hsl,k);
        err_S_full(i) = calc_recovery_err(S_true,W_hsl*diag(b_hsl));
        err_X_obs(i) = calc_recovery_err(X,Z_hsl*A_hsl + W_hsl*diag(b_hsl));
    end
    
    % calculate AUCPR
    aucpr = calc_aucpr(prcurve(:,1),prcurve(:,2));
    
    % combine results
    results = struct('prcurve',prcurve,'aucpr',aucpr,...
        'errLsub',err_L_sub,'errSfull',err_S_full,'errXobs',err_X_obs);
    
%     figure; plot(prcurve(:,2),prcurve(:,1),'o-');
%     xlabel('Recall','FontSize',13); ylabel('Precision','FontSize',13);
%     xlim([0 1]); ylim([0 1]);
%     title('Hybrid SL','FontSize',14);
        
end