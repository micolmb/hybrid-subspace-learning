% Function to compile the results of a set of pr curve experiments
%
% Inputs:
%   resultsDir: directory where individuals results are stored
%   dimVals: values of dimension (p,k) for each experiment
%   sigmaVals: values of noise variance (sigma) for each experiment
%   thetaVals: values of hybrid structure parameter (theta) for each experiment
%   numExp: number of replicate experiments
%   outputDir: directory where compiled results should be saved
%   outputFile: file where compiled results should be saved

function collect_pr_curves(resultsDir,dimVals,sigmaVals,thetaVals,numExp,outputDir,outputFile)

numMethods = 4;
numDims = size(dimVals,1);
numSigmas = size(sigmaVals,1);
numThetas = size(thetaVals,1);

err_X_obs = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
err_L_subspace = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
err_S_full = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
aucpr_S = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
avg_prcurve_S = cell(numMethods,numDims,numSigmas,numThetas);

for dimInd = 1:numDims
    p = dimVals(dimInd,1);
    k = dimVals(dimInd,2);
    for sigmaInd = 1:numSigmas
        sigma = sigmaVals(sigmaInd);
        for thetaInd = 1:numThetas
            theta = thetaVals(thetaInd,:);
            prcurve = cell(numMethods,2);
            for expInd = 1:numExp
                exp = expInd - 1;
                resultsFile = sprintf('results_p%d_k%d_sigma%g_theta-%g-%g-%g_exp%d.mat',p,k,sigma,theta(1),theta(2),theta(3),exp);
                r = load(fullfile(resultsDir,resultsFile));
                results = r.results;
                [~,indO] = max(results.opca.prcurve(:,3));
                [~,indH] = max(results.hsl.prcurve(:,3));
                err_X_obs(:,dimInd,sigmaInd,thetaInd,expInd) = [results.rpca.errXobs results.opca.errXobs(indO) results.spca.errXobs results.hsl.errXobs(indH)];
                err_L_subspace(:,dimInd,sigmaInd,thetaInd,expInd) = [results.rpca.errLsub results.opca.errLsub(indO) results.spca.errLsub results.hsl.errLsub(indH)];
                err_S_full(:,dimInd,sigmaInd,thetaInd,expInd) = [results.rpca.errSfull results.opca.errSfull(indO) -1 results.hsl.errSfull(indH)];
                aucpr_S(:,dimInd,sigmaInd,thetaInd,expInd) = [results.rpca.aucpr results.opca.aucpr results.spca.aucpr results.hsl.aucpr];
                for i = 1:2
                    prcurve{1,i} = [prcurve{1,i} ; results.rpca.prcurve(:,i)'];
                    prcurve{2,i} = [prcurve{2,i} ; results.opca.prcurve(:,i)'];
                    prcurve{3,i} = [prcurve{3,i} ; results.spca.prcurve(:,i)'];
                    prcurve{4,i} = [prcurve{4,i} ; results.hsl.prcurve(:,i)'];
                end
            end
            for m = 1:numMethods
                [avgRec,avgPrec,stdErr] = combine_prcurves(prcurve{m,1},prcurve{m,2});
                avg_prcurve_S{m,dimInd,sigmaInd,thetaInd} = struct('rec',avgRec,'prec',avgPrec,'stderr',stdErr);
            end
        end
    end
end

results = [];
results.errXobs = squeeze(err_X_obs);
results.errLsub = squeeze(err_L_subspace);
results.errSfull = squeeze(err_S_full);
results.aucprS = squeeze(aucpr_S);
results.prcurveS = squeeze(avg_prcurve_S);

save(fullfile(outputDir,outputFile),'results');

end

function [avgRecVals,avgPrecVals,stderrVals] = combine_prcurves(precVals,recVals)

    nRep = size(recVals,1);
    recVals = [ones(nRep,1) recVals zeros(nRep,1)];
    precVals = [zeros(nRep,1) precVals ones(nRep,1)];

    avgRecVals = 1:-0.05:0;
    nVals = length(avgRecVals);
    allPrecVals = zeros(nRep,nVals);
        
    for i = 1:nRep
        for j = 1:nVals
            currRec = avgRecVals(j);
            inds = find(recVals(i,:)==currRec);
            if ~isempty(inds)
                currPrec = mean(precVals(i,inds));
            else
                diff0 = currRec-recVals(i,:);
                diff0(diff0 < 0) = 1;
                diff1 = recVals(i,:)-currRec;
                diff1(diff1 < 0) = 1;
                [~,ind0] = min(diff0);
                [~,ind1] = min(diff1);
                %[~,ind0] = min(max(currRec-recVals(i,:),0));
                %[~,ind1] = min(max(recVals(i,:)-currRec,0));
                x0 = recVals(i,ind0);
                x1 = recVals(i,ind1);
                y0 = precVals(i,ind0);
                y1 = precVals(i,ind1);
                if (x0 == x1)
                    currPrec = (y0+y1)/2;
                else
                    currPrec = y0 + (y1-y0)*(currRec-x0)/(x1-x0);
                end
            end
            allPrecVals(i,j) = currPrec;
        end
    end
       
    avgPrecVals = mean(allPrecVals);
    stderrVals = std(allPrecVals)./sqrt(nRep);
    
end