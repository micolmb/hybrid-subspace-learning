% Function to run a synthetic data experiment comparing hybrid subspace
% learning to several baselines, including:
%   (1) PCA
%   (2) Robust PCA (Candes et al., 2011)
%   (3) Outlier Pursuit (Xu et al., 2010)
%   (4) Sparse PCA (Zou et al., 2006)
%
% Inputs:
%   seed: random generator seed
%   n: number of samples
%   p: total number of features
%   k: number of features for low-rank component
%   sigma: variance of the noise
%   theta: 3-dimensional vector of parameters specifying 
%       (1) the probability that features participate in low-rank component, 
%       (2) the probability that features participate in sparse component, 
%       (3) the probability that features participate in both components 
%   dataType: the type of synthetic data to be generated
%       'hybrid' = data generated directly from hybrid model
%       'outlier' = data generated with hybrid structure and outliers
%       'sparse' = data generated with hybrid structure and extra sparsity in A
%   methodType: the type of hybrid subspace learning method to be used
%       1 = push gamma to gamma_max, enforce no overlap between components
%       2 = select best gamma, allow overlap between components
%   outputDir: directory where results should be saved
%   outputFile: file where results should be saved
%
% Outputs:
%   results: struct containing results of all methods

function results = run_synth_experiment(seed,n,p,k,sigma,theta,dataType,methodType,outputDir,outputFile)

    % add paths
    addpath(genpath('.'));
    
    % set seed
    rng(seed);

    % generate data
    if strcmp(dataType,'hybrid')
        [X_obs,Z_true,A_true,W_true,b_true] = generate_synth_data_hybrid(n,p,k,sigma,theta);
        L_true = Z_true*A_true;
        S_true = W_true*diag(b_true);
        X_true = L_true + S_true;
    elseif strcmp(dataType,'outlier')
        [X_obs,Z_true,A_true,W_true,b_true,O_true] = generate_synth_data_outlier(n,p,k,sigma,theta);
        L_true = Z_true*A_true;
        S_true = W_true*diag(b_true) + O_true;
        X_true = L_true + S_true;
    elseif strcmp(dataType,'sparse')
        [X_obs,Z_true,A_true,W_true,b_true] = generate_synth_data_sparse(n,p,k,sigma,theta);
        L_true = Z_true*A_true;
        S_true = W_true*diag(b_true);
        X_true = L_true + S_true;
    else
        error('Error: The specified data type was not recognized');
    end
    
    % check method
    if methodType == 2
        display('Selecting best gamma is not currently supported. Using methodType = 1.')
    elseif methodType ~= 1
        error('Error: The specified method type was not recognized')
    end
    
    % initialize results
    results = [];

    % run PCA, evaluate results
    results.pca = run_pca_experiment(X_obs,k,X_true,L_true);

    % run Robust PCA, evaluate results
    results.rpca = run_robust_pca_experiment(X_obs,k,X_true,L_true,S_true);

    % run Outlier PCA, evaluate results
    results.opca = run_outlier_pca_experiment(X_obs,k,X_true,L_true,S_true);

    % run Sparse PCA, evaluate the results
    results.spca = run_sparse_pca_experiment(X_obs,k,X_true,L_true,S_true);

    % run Hybrid SL, evaluate the results
    results.hsl = run_hybrid_sl_experiment(X_obs,k,X_true,L_true,S_true);
    
    % save results
    save(fullfile(outputDir,outputFile)); 

end

% Function to run PCA on X and evaluate the results
function [results] = run_pca_experiment(X,k,X_true,L_true)

    % run PCA
    [A_pca,Z_pca] = pca(X,'Center',false,'NumComponent',k);
    A_pca = A_pca';

    % measure recovery of low-rank component
    L_ind_true = sum(abs(L_true)) ~= 0;
    L_pca = Z_pca*A_pca;
    err_L_subspace = calc_subspace_err(L_true,L_pca,k);
    err_L_full = calc_recovery_err(L_true,L_pca);
    err_L_part = calc_recovery_err(L_true(:,L_ind_true),L_pca(:,L_ind_true));
    
    % measure recovery of true data
    X_pca = L_pca;
    err_X_true = calc_recovery_err(X_true,X_pca);
    
    % measure recovery of observed data
    err_X_obs = calc_recovery_err(X,X_pca);

    % combine results
    results = struct('errXobs',err_X_obs,'errXtrue',err_X_true,...
        'errLsub',err_L_subspace,'errLfull',err_L_full,'errLpart',err_L_part);

end

% Function to run Robust PCA on X and evaluate the results
function [results] = run_robust_pca_experiment(X,k,X_true,L_true,S_true)

    % run Robust PCA, use default lambda = 1/sqrt(max(n,p))
    %[L_rpca,S_rpca] = robustpca(X);
    
    % run Robust PCA, choose oracle parameter
    [L_rpca,S_rpca] = robust_pca_wrapper_oracle(X,k,L_true);

    % measure recovery of low-rank component
    L_ind_true = sum(abs(L_true)) ~= 0;
    err_L_subspace = calc_subspace_err(L_true,L_rpca,k);
    err_L_full = calc_recovery_err(L_true,L_rpca);
    err_L_part = calc_recovery_err(L_true(:,L_ind_true),L_rpca(:,L_ind_true));    

    % measure recovery of sparse component
    S_ind_true = sum(abs(S_true)) ~= 0;
    err_S_full = calc_recovery_err(S_true,S_rpca);
    err_S_part = calc_recovery_err(S_true(:,S_ind_true),S_rpca(:,S_ind_true));

    % measure recovery of true data
    X_rpca = L_rpca + S_rpca;
    err_X_true = calc_recovery_err(X_true,X_rpca);
    
    % measure recovery of observed data
    err_X_obs = calc_recovery_err(X,X_rpca);

    % measure assignment of sparse features
    S_ind_rpca = sqrt(sum(S_rpca.^2)) > mean(sqrt(sum(S_rpca.^2)));
    [prec_S,rec_S,f1_S] = calc_assignment_err(S_ind_true,S_ind_rpca);
    
    % combine results
    results = struct('errXobs',err_X_obs,'errXtrue',err_X_true,...
        'errLsub',err_L_subspace,'errLfull',err_L_full,'errLpart',err_L_part,...
        'errSfull',err_S_full,'errSpart',err_S_part,'precS',prec_S,'recS',rec_S,'f1S',f1_S);

end

% Function to run Outlier PCA on X and evaluate the results
function [results] = run_outlier_pca_experiment(X,k,X_true,L_true,S_true)

    % run Outlier PCA
    %if numOutlier == 0
    %    lambda = 3/(7*eps);
    %else
    %    lambda = 3/(7*sqrt(numOutlier));
    %end
    %[L_opca,S_opca] = mrpca(X,ones(size(X)),lambda,'ini_rank',k);
    
    % run Outlier PCA, choose oracle parameter
    [L_opca,S_opca] = outlier_pca_wrapper_oracle(X,k,L_true);
    
    % measure recovery of low-rank component
    L_ind_true = sum(abs(L_true)) ~= 0;
    err_L_subspace = calc_subspace_err(L_true,L_opca,k);
    err_L_full = calc_recovery_err(L_true,L_opca);
    err_L_part = calc_recovery_err(L_true(:,L_ind_true),L_opca(:,L_ind_true));    

    % measure recovery of sparse component
    S_ind_true = sum(abs(S_true)) ~= 0;
    err_S_full = calc_recovery_err(S_true,S_opca);
    err_S_part = calc_recovery_err(S_true(:,S_ind_true),S_opca(:,S_ind_true));

    % measure recovery of true data
    X_opca = L_opca + S_opca;
    err_X_true = calc_recovery_err(X_true,X_opca);
    
    % measure recovery of observed data
    err_X_obs = calc_recovery_err(X,X_opca);

    % measure assignment of sparse features
    S_ind_opca = sum(abs(S_opca)) ~= 0;
    [prec_S,rec_S,f1_S] = calc_assignment_err(S_ind_true,S_ind_opca);
    
    % combine results
    results = struct('errXobs',err_X_obs,'errXtrue',err_X_true,...
        'errLsub',err_L_subspace,'errLfull',err_L_full,'errLpart',err_L_part,...
        'errSfull',err_S_full,'errSpart',err_S_part,'precS',prec_S,'recS',rec_S,'f1S',f1_S);

end

% Function to run Sparse PCA on X and evaluate the results
function [results] = run_sparse_pca_experiment(X,k,X_true,L_true,S_true)

    % run Sparse PCA
    %colNorms = sqrt(sum(X.^2));
    %Xstd = X./repmat(colNorms,size(X,1),1);
    %Ap_spca = spca(Xstd,[],k,Inf,-numFeature);
    %A_spca = pinv(Ap_spca);
    %Z_spca = Xstd*Ap_spca;
    %L_spca = (Z_spca*A_spca).*repmat(colNorms,size(X,1),1);
    
    % run Sparse PCA, choose oracle parameter
    [L_spca,Ap_spca] = sparse_pca_wrapper_oracle(X,k,L_true);

    % measure recovery of low-rank component
    L_ind_true = sum(abs(L_true)) ~= 0;
    err_L_subspace = calc_subspace_err(L_true,L_spca,k);
    err_L_full = calc_recovery_err(L_true,L_spca);
    err_L_part = calc_recovery_err(L_true(:,L_ind_true),L_spca(:,L_ind_true));    

    % measure recovery of true data
    X_spca = L_spca;
    err_X_true = calc_recovery_err(X_true,X_spca);
    
    % measure recovery of observed data
    err_X_obs = calc_recovery_err(X,X_spca);
    
    % measure assignment of sparse features
    S_ind_true = sum(abs(S_true)) ~= 0;
    S_ind_spca = (sum(abs(Ap_spca),2) == 0)';
    [prec_S,rec_S,f1_S] = calc_assignment_err(S_ind_true,S_ind_spca);
    
    % combine results
    results = struct('errXobs',err_X_obs,'errXtrue',err_X_true,...
        'errLsub',err_L_subspace,'errLfull',err_L_full,'errLpart',err_L_part,...
        'precS',prec_S,'recS',rec_S,'f1S',f1_S);
            
end

% Function to run Hybrid SL on X and evaluate the results
function [results] = run_hybrid_sl_experiment(X,k,X_true,L_true,S_true)

    % run Hybrid SL
    %if methodType == 1 % choose max gamma
    %    [Z_hsl,A_hsl,W_hsl,b_hsl] = hybrid_sl_select_params(X,k,[],struct('gamma','max'));
    %elseif methodType == 2 % choose best gamma
    %    [Z_hsl,A_hsl,W_hsl,b_hsl] = hybrid_sl_select_params(X,k,[],struct('gamma','best'));
    %end
    
    % run Hybrid SL, choose oracle parameter
    [Z_hsl,A_hsl,W_hsl,b_hsl] = hybrid_sl_wrapper_oracle(X,k,L_true,S_true);
    L_hsl = Z_hsl*A_hsl;
    S_hsl = W_hsl*diag(b_hsl);
    
    % measure recovery of low-rank component
    L_ind_true = sum(abs(L_true)) ~= 0;
    err_L_subspace = calc_subspace_err(L_true,L_hsl,k);
    err_L_full = calc_recovery_err(L_true,L_hsl);
    err_L_part = calc_recovery_err(L_true(:,L_ind_true),L_hsl(:,L_ind_true));    

    % measure recovery of sparse component
    S_ind_true = sum(abs(S_true)) ~= 0;
    err_S_full = calc_recovery_err(S_true,S_hsl);
    err_S_part = calc_recovery_err(S_true(:,S_ind_true),S_hsl(:,S_ind_true));

    % measure recovery of true data
    X_hsl = L_hsl + S_hsl;
    err_X_true = calc_recovery_err(X_true,X_hsl);
    
    % measure recovery of observed data
    err_X_obs = calc_recovery_err(X,X_hsl);    
    
    % measure assignment of sparse features
    S_ind_true = sum(abs(S_true)) ~= 0;
    S_ind_hsl = sum(abs(S_hsl)) ~= 0;
    [prec_S,rec_S,f1_S] = calc_assignment_err(S_ind_true,S_ind_hsl);
    
    % combine results
    results = struct('errXobs',err_X_obs,'errXtrue',err_X_true,...
        'errLsub',err_L_subspace,'errLfull',err_L_full,'errLpart',err_L_part,...
        'errSfull',err_S_full,'errSpart',err_S_part,'precS',prec_S,'recS',rec_S,'f1S',f1_S);
    
end
