% Function to compile the results of a phase transition experiment
%
% Inputs:
%   resultsDir: directory where individuals results are stored
%   kVals: values of low-rank dimension k for each experiment
%   thetaVals: values of low-rank fraction theta1 for each experiment
%   numExp: number of replicate experiments
%   outputDir: directory where compiled results should be saved
%   outputFile: file where compiled results should be saved

function collect_phase_transition(resultsDir,kVals,thetaVals,numExp,outputDir,outputFile)

numMethods = 3;
numKs = size(kVals,1);
numThetas = size(thetaVals,1);

err_L_subspace = zeros(numMethods,numKs,numThetas,numExp);
f1_S = zeros(numMethods,numKs,numThetas,numExp);

for kInd = 1:numKs
    k = kVals(kInd);
    for thetaInd = 1:numThetas
        theta = thetaVals(thetaInd,:);
        for expInd = 1:numExp
            exp = expInd-1;
            resultsFile = sprintf('results_p200_k%d_sigma0_theta-%g-%g-0_exp%d.mat',k,theta(1),theta(2),exp);
            r = load(fullfile(resultsDir,resultsFile));
            results = r.results;
            err_L_subspace(:,kInd,thetaInd,expInd) = [results.rpca.errLsub results.opca.errLsub results.hsl.errLsub];
            f1_S(:,kInd,thetaInd,expInd) = [results.rpca.f1S results.opca.f1S results.hsl.f1S];
        end
    end
end

results = [];
results.errLsub = squeeze(err_L_subspace);
results.f1S = squeeze(f1_S);

save(fullfile(outputDir,outputFile),'results');

end