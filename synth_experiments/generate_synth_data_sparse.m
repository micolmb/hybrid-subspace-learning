% Function to generate synthetic data according to the hybrid low-rank +
% high-dimensional model, but with extra sparsity in the loading matrix
%
% Inputs:
%   n: number of samples
%   p: dimensionality of each view
%   k: dimensionality of shared latent space
%   sigma: variance of noise in observed features
%   theta: 3-dimensional vector of parameters specifying 
%   (1) the probability that features participate in low-rank component, 
%   (2) the probability that features participate in sparse component, 
%   (3) the probability that features participate in both components 
%
% Outputs:
%   X: n-by-p matrix of observed features
%   Z: n-by-k matrix of low-rank component features
%   A: k-by-p matrix of low-rank component coefficients
%   W: n-by-p matrix of high-dim component features
%   b: p-by-1 vector of high-dim component coefficients
%   Ap: p-by-k matrix of sparse loadings

function [X,Z,A,W,b,Ap] = generate_synth_data_sparse(n,p,k,sigma,theta)

    % generate low-rank component features
    Z = mvnrnd(zeros(n,k),eye(k));
    %nrms = sqrt(sum(Z.^2,2)./size(Z,2));
    %Z = Z./repmat(nrms,1,size(Z,2));
    Z = lF_project(Z);

    % generate low-rank component coefficients with sparse loadings
    A = .5 + rand(k,p);
    sgnChange = rand(k,p) < 0.5;
    A(sgnChange) = -1.*A(sgnChange);
    Ap = A'*inv(A*A');
    Ap(rand(k,p) < .5) = 0; % sparsity fixed at 50% of entries
    A = inv(Ap'*Ap)*Ap';
    
    % generate sparse component features
    W = mvnrnd(zeros(n,p),eye(p));
    %nrms = sqrt(sum(W.^2,2)./size(W,2));
    %W = W./repmat(nrms,1,size(W,2));
    W = lF_project(W);
    
    % generate sparse component coefficients
    b = sqrt(k)*(.5 + rand(p,1));
    sgnChange = rand(p,1) < 0.5;
    b(sgnChange) = -1.*b(sgnChange);
    
    % add structured sparsity to both sets of coefficients
    r = rand(1,p);
    t = cumsum(theta);
    lr_ind = r <= t(1);
    sp_ind = r > t(1) & r <= t(2);
    bth_ind = r > t(2) & r <= t(3);
    A(:,~(lr_ind|bth_ind)) = 0;
    Ap(~(lr_ind|bth_ind),:) = 0;
    b(~(sp_ind|bth_ind)) = 0;
    
    % generate observed features
    mu = Z*A + W*diag(b);
    eps = mvnrnd(zeros(n,p),sigma*eye(p));
    X = mu + eps;

end