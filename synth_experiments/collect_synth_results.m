% Function to compile the results of a set of synthetic experiments
%
% Inputs:
%   resultsDir: directory where individuals results are stored
%   dimVals: values of dimension (p,k) for each experiment
%   sigmaVals: values of noise variance (sigma) for each experiment
%   thetaVals: values of hybrid structure parameter (theta) for each experiment
%   numExp: number of replicate experiments
%   outputDir: directory where compiled results should be saved
%   outputFile: file where compiled results should be saved

function collect_synth_results(resultsDir,dimVals,sigmaVals,thetaVals,numExp,outputDir,outputFile)

numMethods = 5;
numDims = size(dimVals,1);
numSigmas = size(sigmaVals,1);
numThetas = size(thetaVals,1);

err_X_obs = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
err_X_true = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
err_L_subspace = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
err_L_full = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
err_L_part = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
err_S_full = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
err_S_part = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
prec_S = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
rec_S = zeros(numMethods,numDims,numSigmas,numThetas,numExp);
f1_S = zeros(numMethods,numDims,numSigmas,numThetas,numExp);

for dimInd = 1:numDims
    p = dimVals(dimInd,1);
    k = dimVals(dimInd,2);
    for sigmaInd = 1:numSigmas
        sigma = sigmaVals(sigmaInd);
        if length(unique(dimVals(:,2))) > 1
            sigma = (sigma/20)*k;
        end
        for thetaInd = 1:numThetas
            theta = thetaVals(thetaInd,:);
            for expInd = 1:numExp
                exp = expInd - 1;
                if length(unique(thetaVals(:,3))) > 1 && theta(3) ~= 1
                    resultsFile = sprintf('results_p%d_k%d_sigma%g_theta-%.2f-%.2f-%g_exp%d.mat',p,k,sigma,theta(1),theta(2),theta(3),exp);
                else
                    resultsFile = sprintf('results_p%d_k%d_sigma%g_theta-%g-%g-%g_exp%d.mat',p,k,sigma,theta(1),theta(2),theta(3),exp);
                end
                r = load(fullfile(resultsDir,resultsFile));
                results = r.results;
                err_X_obs(:,dimInd,sigmaInd,thetaInd,expInd) = [results.pca.errXobs results.rpca.errXobs results.opca.errXobs results.spca.errXobs results.hsl.errXobs];
                err_X_true(:,dimInd,sigmaInd,thetaInd,expInd) = [results.pca.errXtrue results.rpca.errXtrue results.opca.errXtrue results.spca.errXtrue results.hsl.errXtrue];
                err_L_subspace(:,dimInd,sigmaInd,thetaInd,expInd) = [results.pca.errLsub results.rpca.errLsub results.opca.errLsub results.spca.errLsub results.hsl.errLsub];
                err_L_full(:,dimInd,sigmaInd,thetaInd,expInd) = [results.pca.errLfull results.rpca.errLfull results.opca.errLfull results.spca.errLfull results.hsl.errLfull];
                err_L_part(:,dimInd,sigmaInd,thetaInd,expInd) = [results.pca.errLpart results.rpca.errLpart results.opca.errLpart results.spca.errLpart results.hsl.errLpart];
                err_S_full(:,dimInd,sigmaInd,thetaInd,expInd) = [-1 results.rpca.errSfull results.opca.errSfull -1 results.hsl.errSfull];
                err_S_part(:,dimInd,sigmaInd,thetaInd,expInd) = [-1 results.rpca.errSpart results.opca.errSpart -1 results.hsl.errSpart];
                prec_S(:,dimInd,sigmaInd,thetaInd,expInd) = [-1 results.rpca.precS results.opca.precS results.spca.precS results.hsl.precS];
                rec_S(:,dimInd,sigmaInd,thetaInd,expInd) = [-1 results.rpca.recS results.opca.recS results.spca.recS results.hsl.recS];
                f1_S(:,dimInd,sigmaInd,thetaInd,expInd) = [-1 results.rpca.f1S results.opca.f1S results.spca.f1S results.hsl.f1S];
            end
        end
    end
end

results = [];
results.errXobs = squeeze(err_X_obs);
results.errXtrue = squeeze(err_X_true);
results.errLsub = squeeze(err_L_subspace);
results.errLfull = squeeze(err_L_full);
results.errLpart = squeeze(err_L_part);
results.errSfull = squeeze(err_S_full);
results.errSpart = squeeze(err_S_part);
results.precS = squeeze(prec_S);
results.recS = squeeze(rec_S);
results.f1S = squeeze(f1_S);

save(fullfile(outputDir,outputFile),'results');

end