% Function to generate synthetic data according to the hybrid low-rank +
% high-dimensional model
%
% Inputs:
%   n: number of samples
%   p: dimensionality of each view
%   k: dimensionality of shared latent space
%   sigma: variance of noise in observed features
%   theta: 3-dimensional vector of parameters specifying 
%   (1) the probability that features participate in low-rank component, 
%   (2) the probability that features participate in sparse component, 
%   (3) the probability that features participate in both components 
%
% Outputs:
%   X: n-by-p matrix of observed features
%   Z: n-by-k matrix of low-rank component features
%   A: k-by-p matrix of low-rank component coefficients
%   W: n-by-p matrix of high-dim component features
%   b: p-by-1 vector of high-dim component coefficients

function [X,Z,A,W,b] = generate_synth_data_hybrid(n,p,k,sigma,theta)

    % generate low-rank component features
    Z = mvnrnd(zeros(n,k),eye(k));
    %nrms = max(sqrt(sum(Z.^2,2)./size(Z,2)),1);
    %Z = Z./repmat(nrms,1,size(Z,2));
    Z = lF_project(Z);

    % generate low-rank component coefficients
    A = .5 + rand(k,p);
    sgnChange = rand(k,p) < 0.5;
    A(sgnChange) = -1.*A(sgnChange);
    
    % generate sparse component features
    W = mvnrnd(zeros(n,p),eye(p));
    %nrms = sqrt(sum(W.^2,2)./size(W,2));
    %W = W./repmat(nrms,1,size(W,2));
    W = lF_project(W);
    
    % generate sparse component coefficients
    %range = sqrt(k) + rand(p,1)*(sqrt(k)/2);
    range = sqrt(k);
    b = range.*(.5 + rand(p,1));
    sgnChange = rand(p,1) < 0.5;
    b(sgnChange) = -1.*b(sgnChange);
    
    % add structured sparsity to both sets of coefficients
    r = rand(1,p);
    t = cumsum(theta);
    lr_ind = r <= t(1);
    sp_ind = r > t(1) & r <= t(2);
    bth_ind = r > t(2) & r <= t(3);
    A(:,~(lr_ind|bth_ind)) = 0;
    b(~(sp_ind|bth_ind)) = 0;
    
    % generate observed features
    X_mu = Z*A + W*diag(b);
    X_eps = mvnrnd(zeros(n,p),sigma*eye(p));
    X = X_mu + X_eps;
    
    %temp = var(X);
    %figure; hold on;
    %plot(find(b==0),temp(b==0),'bo-');
    %plot(find(b~=0),temp(b~=0),'r*');
    %drawnow;
            
end